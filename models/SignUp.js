var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');

// var MilanSchema = new mongoose.Schema({
//   companyid:{ type: mongoose.Schema.Types.ObjectId },
//   firstname: {type: String, default:"",minlength: 0, maxlength: 60,},
//   lastname: {type: String, default:"",minlength: 0, maxlength: 60,},
//   emailaddress: {type: String, default:"",minlength: 0, maxlength: 60,},
//   otp: {type: String, default:"aidfretr",minlength: 0, maxlength: 30,},
//   attempts: { type: Number, min: 0 ,default:1},
//   passattempts: { type: Number, min: 0 ,default:0},
//   failattempts: { type: Number, min: 0 ,default:0},
//   verified: {type:Boolean,default:false},
//   isActive:{type:Boolean,default:false},
//   isChecked:{type:Boolean,default:false},
//   pdfUrl:{type: String, default:"",minlength: 0, maxlength: 255},
//   createdat : { type: Date, default: Date.now },
//   steps: [{
//     imageUrl: {type: String, default:"",minlength: 0, maxlength: 255},
//     passfail:  {type:Boolean,default:false},
//     comment: {type: String, default:"",minlength: 0, maxlength: 255},
//     createdat : { type: Date, default: Date.now }
//   }],
// });

var SignUpSchema = new mongoose.Schema({
  firstname: {type: String, default:"",minlength: 0, maxlength: 100,},
  lastname: {type: String, default:"",minlength: 0, maxlength: 100,},
  email: {type: String, default:"",minlength: 0, maxlength: 100,},
  password: {type: String, default:"",minlength: 0, maxlength: 60,}
});
// var LoginSchema = new mongoose.Schema({
//   user_name: {type: String, default:"",minlength: 0, maxlength: 60,},
//   password: {type: String, default:"",minlength: 0, maxlength: 60,}
// });
//
// MilanSchema.methods.getAllEvaluations = function(cb){
//   return this.model('Company').find({},{verified:"verified",lastname:"lastname",firstname:"firstname",emailaddress:"emailaddress",supervisoremailid:"supervisoremailid",companyname:"companyname"}, cb);
// };
//
// MilanSchema.methods.getSupervisorEvaluations =function (obj,cb) {
//   return this.model('Company').find({supervisoremailid:obj.supervisoremailid}, cb);
// }
//
// MilanSchema.methods.checkExist = function(obj,cb) {
//   // console.log(this.model('Company').find({ email: this.email }, cb));
//   return this.model('Milan').find({ companyid: obj.companyid,_id:obj.Milanid }, cb);
// };
// //
// // MilanSchema.methods.alreadyExist = function(cb) {
// //   // console.log(this.model('Company').find({ email: this.email }, cb));
// //   return this.model('Company').count({ emailaddress: this.emailaddress }, cb);
// // };
// //
// MilanSchema.methods.verifyOtp = function(otpObj,cb) {
//   // return this.model('Milan').find({_id:otpObj.regid,otp:this.otp,emailaddress:this.emailaddress}, cb);
//   return this.model('Milan').find({_id:otpObj.regid,otp:this.otp}, cb);
// };

mongoose.model('SignUp', SignUpSchema);
// mongoose.model('Login', LoginSchema);
