var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');

var SuperAdminSchema = new mongoose.Schema({
  name:{ type: String, minlength: 0, maxlength: 30,default:""},
  email:{ type: String, minlength: 0, maxlength: 60,default:""},
  password:{ type: String, minlength: 0, maxlength: 30,default:""},
  role:{ type: String, minlength: 0, maxlength: 30,default:"superadmin"},
  createdat:{type:Date,default: Date.now}
});

SuperAdminSchema.methods.alreadyExist = function(superadmin,cb) {
  // console.log(this.model('superadmin').count({ email: this.email }, cb));
  return this.model('SuperAdmin').find({ email: superadmin.email }, cb);
};

//check name or email-id & password for login
SuperAdminSchema.methods.checkLogin = function(superadmin,cb) {
  // console.log(this.model('superadmin').count({ email: this.email }, cb));
  // return this.model('superadmin').findOne({ superadminemailid: superadmin.superadminemailid }, cb);
  // return this.model('SuperAdmin').findOne({$or:[{name:superadmin.email},{superadminemailid: superadmin.superadminemailid}],password:superadmin.password}, cb);
  return this.model('SuperAdmin').findOne({email: superadmin.email,password:superadmin.password}, cb);
};

SuperAdminSchema.methods.changePassword = function(newpwd,cb) {
  this.password = newpwd;
  this.save(cb);
};


mongoose.model('SuperAdmin', SuperAdminSchema);
