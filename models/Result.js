var mongoose = require('mongoose');
var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');

var ResultSchema = new mongoose.Schema({
  companyid:{ type: mongoose.Schema.Types.ObjectId },
  userid:{ type: mongoose.Schema.Types.ObjectId },
  emailaddress: {type: String, default:"",minlength: 0, maxlength: 60,},
  attempts: { type: Number, min: 0 ,default:0},
  passattempts: { type: Number, min: 0 ,default:0},
  failattempts: { type: Number, min: 0 ,default:0},
  createdat : { type: Date, default: Date.now },
  updatedat : { type: Date, default: Date.now }
});


mongoose.model('Result', ResultSchema);
