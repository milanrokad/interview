var mongoose = require('mongoose');

var StepSchema = new mongoose.Schema({
    _tutorial: {
        type: String,
        ref: 'Tutorial'
    },
    "image": {
        type: String,
        default: ""
    },
    "comment": {
        type: String,
        minlength: 0,
        maxlength: 255,
        default: ""
    },
    "pass_fail": Number,
    "stepnum": Number,
    "updatedat": {
        type: Date,
        default: Date.now
    },
    "createdat": {
        type: Date,
        default: Date.now
    }

});

var ObjectId = require('mongoose').Types.ObjectId;

StepSchema.methods.getSteps = function(obj,cb) {
  return this.model('Step').find({_tutorial: obj.createrid}, cb);
};

mongoose.model('Step', StepSchema);
