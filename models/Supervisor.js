var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');

var SupervisorSchema = new mongoose.Schema({
  supervisorname:{ type: String, minlength: 0, maxlength: 30,default:""},
  supervisoremailid:{ type: String, minlength: 0, maxlength: 60,default:""},
  password:{ type: String, minlength: 0, maxlength: 30,default:""},
    role:{ type: String, minlength: 0, maxlength: 30,default:""},
    isActive:{type:Boolean,default:true},
  createdat:{type:Date,default: Date.now}
});

SupervisorSchema.methods.alreadyExist = function(supervisor,cb) {
  // console.log(this.model('Supervisor').count({ email: this.email }, cb));
  return this.model('Supervisor').find({ supervisoremailid: supervisor.supervisoremailid }, cb);
};

//check name or email-id & password for login
SupervisorSchema.methods.checkLogin = function(supervisor,cb) {
  // console.log(this.model('Supervisor').count({ email: this.email }, cb));
  // return this.model('Supervisor').findOne({ supervisoremailid: supervisor.supervisoremailid }, cb);
  return this.model('Supervisor').findOne({$or:[{supervisorname:supervisor.supervisoremailid},{supervisoremailid: supervisor.supervisoremailid}],password:supervisor.password}, cb);
};

SupervisorSchema.methods.changeSupervisorState= function(cb) {
  this.isActive =! this.isActive;
  this.save(cb);
};

SupervisorSchema.methods.changePassword = function(newpwd,cb) {
  this.password = newpwd;
  this.save(cb);
};


mongoose.model('Supervisor', SupervisorSchema);
