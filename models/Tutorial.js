var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');
// var nameValidator = [
//   validate({
//     validator: 'isLength',
//     arguments: [0, 30],
//     message: 'ComapnyName should be between {ARGS[0]} and {ARGS[1]} characters'
//   })
// ];
//
//
// var emailValidator = [
//   validate({
//     validator: 'isLength',
//     arguments: [0, 30],
//     message: 'ComapnyName should be between {ARGS[0]} and {ARGS[1]} characters'
//   })
// ];
//
// var validateEmail = function(email) {
//     var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//     return re.test(email)
// };

var TutorialSchema = new mongoose.Schema({
  companyname:{ type: String, minlength: 0, maxlength: 30,default:""},
  supervisoremailid:{ type: String, minlength: 0, maxlength: 60,default:""},
  emailaddress:{ type: String, minlength: 0, maxlength: 60,default:""},
  firstname:{ type: String, minlength: 0, maxlength: 30,default:""},
  lastname:{ type: String, minlength: 0, maxlength: 30,default:""}
});


TutorialSchema.methods.alreadyExist = function(cb) {
  // console.log(this.model('Company').count({ email: this.email }, cb));
  return this.model('Tutorial').count({ email: this.email }, cb);
};

mongoose.model('Tutorial', TutorialSchema);
