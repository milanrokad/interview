var mongoose = require('mongoose');
var validate = require('mongoose-validator');
var error = require('../config/errors.js');


var CompanySchema = new mongoose.Schema({
  companyname:{ type: String, minlength: 0, maxlength: 30,default:""},
  users: [{
    name: {type: String, default:"",minlength: 0, maxlength: 30,},
    // fname: {type: String, default:"",minlength: 0, maxlength: 60,},
    // lname: {type: String, default:"",minlength: 0, maxlength: 60,},
    email: {type: String, default:"",minlength: 0, maxlength: 60,},
    password: {type: String, default:"",minlength: 0, maxlength: 30,},
    role: {type: String, default:"",minlength: 0, maxlength: 30,},
    defaultreviewer: {type: String, default:"",minlength: 0, maxlength: 30,},
    accessToken:{type: String, default:"",minlength: 0, maxlength: 60,},
    isPasswordVerified :{type:Boolean,default:true},
    linkvisited:{type:Boolean,default:false},
    passwordLinkGeneratedAt: { type: Date, default: Date.now },
    isActive:{type:Boolean,default:true},
    createdat : { type: Date, default: Date.now }
  }],
  isActive:{type:Boolean,default:true},
  createdat : { type: Date, default: Date.now }
});
//
// CompanySchema.methods.getAllEvaluations = function(cb){
//   return this.model('Company').find({},{verified:"verified",lastname:"lastname",firstname:"firstname",emailaddress:"emailaddress",supervisoremailid:"supervisoremailid",companyname:"companyname"}, cb);
// };
//
// CompanySchema.methods.getSupervisorEvaluations =function (obj,cb) {
//   return this.model('Company').find({supervisoremailid:obj.supervisoremailid}, cb);
// }
//
// CompanySchema.methods.checkExist = function(obj,cb) {
//   // console.log(this.model('Company').find({ email: this.email }, cb));
//   return this.model('Company').find({ emailaddress: obj.emailaddress }, cb);
// };
//
CompanySchema.methods.alreadyExist = function(Obj,cb) {
  // console.log(this.model('Company').find({ email: this.email }, cb));
  return this.model('Company').find({_id:Obj.companyid}, cb);
  // return this.model('Company').count({ emailaddress: this.emailaddress }, cb);
};

CompanySchema.methods.changeCompanyState= function(cb) {
  this.isActive =! this.isActive;
  this.save(cb);
};

CompanySchema.methods.changeCompanyName = function(obj,cb){
  this.companyname = obj.companyname;
  this.save(cb);
}
//
// CompanySchema.methods.verifyOtp = function(otpObj,cb) {
//   return this.model('Company').find({_id:otpObj.regid,otp:this.otp }, cb);
// };

mongoose.model('Company', CompanySchema);
