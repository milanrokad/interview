
const port = process.env.PORT || 3000;

const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
// const mongoose = require('mongoose');
// var uri = 'mongodb://mlabdb:Mlab@123@ds043991.mlab.com:43991/slotsdb';
// // var conn = mongoose.createConnection(uri);
// // mongoose.connect('mongodb://mlabdb:Mlab@123@ds043991.mlab.com:43991/slotsdb');
// var options = {
//     "server" : {
//       "socketOptions" : {
//         "keepAlive" : 300000,
//         "connectTimeoutMS" : 30000
//       }
//     },
//     "replset" : {
//       "socketOptions" : {
//         "keepAlive" : 300000,
//         "connectTimeoutMS" : 30000
//       }
//     }
//   }

// mongoose.connect(uri,options);
// var conn = mongoose.connection;

// conn.on('error', console.error.bind(console, 'connection error:'));

// conn.once('open', function() {
//     // Wait for the database connection to establish, then start the app.
//     console.log("database open");
//     // require('./src/tutorialApi')(app,mongoose,nodemailerutil,ses,passport,jwt,crypto,fs,error.errors);
// });

app.use(express.static(__dirname));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());


require('./src/tutorialApi')(app);

app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`);
});