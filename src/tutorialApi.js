var jsonData = [
    { id: 1, slot: '12am to 2am', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 2, slot: '2am to 4am', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 3, slot: '4am to 6am', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 4, slot: '6am to 8am', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 5, slot: '8am to 10am', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 6, slot: '10am to 12pm', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 7, slot: '12pm to 2pm', haveDetail: false, f_name: '', l_name: '', contact: '' },
    { id: 8, slot: '2pm to 4pm', haveDetail: false, f_name: '', l_name: '', contact: '' }
];

module.exports = function (app) {
    app.get('/', function (req, res) { // when run project directly move to home page
        res.redirect('/index.html');
    });

    // get slot list from fake data
    app.get('/getSlots', function (req, res) { // when run project directly move to home page
        res.json({ responseCode: 0, responseMsg: 'Slots get successfully', responseData: jsonData });
    });
    
    // save slot detail
    app.post('/saveDetail', function (req, res) { // when run project directly move to home page
        console.log(req.body);
        if (req.body.f_name != '' || req.body.l_name != '' || req.body.contact != '') {
            for (let index = 0; index < jsonData.length; index++) {
                if (req.body.id == jsonData[index].id) {
                    jsonData[index].f_name = req.body.f_name;
                    jsonData[index].l_name = req.body.l_name;
                    jsonData[index].contact = req.body.contact;
                    jsonData[index].haveDetail = true;
                    console.log(jsonData);
                    res.json({ responseCode: 0, responseMsg: 'Detail added successfully', responseData: jsonData });
                }
            }
        }
        else {
            for (let index = 0; index < jsonData.length; index++) {
                if (req.body.id == jsonData[index].id) {
                    jsonData[index].haveDetail = false;
                    console.log(jsonData);
                    res.json({ responseCode: 1, responseMsg: 'Detail not added', responseData: jsonData });
                }
            }

        }


    });
};
