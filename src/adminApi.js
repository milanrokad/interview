var async = require("async");
var s3fs = require("s3fs");
var PDFDocument = require('pdfkit'); // add pdfkit module to access it
var htmlToPdf = require('html-to-pdf');
var AWS = require('aws-sdk'),
    fs = require('fs');
AWS.config.update({
    accessKeyId: 'AKIAIZYVRLJ3VCHSRYZA',
    secretAccessKey: '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'
});
module.exports = function(app, mongoose, nodemailerutil, ses, sesClient, passport, jwt, crypto, fs, error) {
    require('../models/Company');
    require('../models/Step');
    require('../models/Tutorial');
    require('../models/Supervisor');
    require('../models/SuperAdmin');
    require('../models/User');
    require('../models/Result');

    var Company = mongoose.model('Company');
    var Step = mongoose.model('Step');
    var Tutorial = mongoose.model('Tutorial');
    var Supervisor = mongoose.model('Supervisor');
    var SuperAdmin = mongoose.model('SuperAdmin');
    var User = mongoose.model('User');
    var Result = mongoose.model('Result');

    function generatePassword() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4();
    }

    function capitalize(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }

    // template that has been used for Send Mail
    function getNewPasswordMailTemplate(newpassword) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>New Password</title></head>';
        verificationTemplate += '<body><div style="color: #000;">';
        // verificationTemplate += '<p style="color: #000;>New Password for CommScope</p></div></br>';
        verificationTemplate += '<div><ul style="list-style-type:none;margin:0 auto;">';
        verificationTemplate += '<li style="color: #000;><span>Dear CommScope user,</span></li>';
        verificationTemplate += '<li style="color: #000;><p>Please use this  <span style="font-weight:bold;">' + newpassword + '</span> as new password for login.</p></li>';
        verificationTemplate += '<li style="color: #000;><p>Thank you.</p></li>';
        // verificationTemplate += '<li style="margin-top: 20px;"><span style="style="color: #000; text-decoration: none;font-weight: bold;">' + newpassword + '</span></li>';
        verificationTemplate += '</ul></div><div style="background-color: #F5F5F5;padding-bottom:10px;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }


    // template that has been used for Send Mail
    function getSendResetPasswordMailTemplate(host, obj) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>New Password</title></head>';
        verificationTemplate += '<body><div style="color: #000;">';
        verificationTemplate += '<p style="font-weight: bold;">Reset password Message by CommScope team</p></div></br>';
        verificationTemplate += '<div>';
        // verificationTemplate += '<li><span>Dear CommScope Drop Craft User,</span></li>';
        // verificationTemplate += '<li><span></span></li>';
        // verificationTemplate += '<p style="margin-top:10px;"><a href="' + host + '/#/resetpassword/' + obj.reviewerid + '/' + obj.companyid + '">Please use this link to set new password for login.</a></p>';
        verificationTemplate += '<p style="margin:10px;"><a href="http://commscope.xpertuniversity.com/#/resetpassword/' + obj.reviewerid + '/' + obj.companyid + '">Click here to reset your password.</a></p>';
        // verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; background-color: #DF0003; text-decoration: none;  color: #fff;font-weight: bold;">' + newpassword + '</p></li>';
        verificationTemplate += '</div><div style="background-color: #F5F5F5;padding-bottom: 10px;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }


    // template that has been used for Send Mail with accessToken
    function getSendChangePasswordLinkTemplate(host, obj, accesstoken) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>New Password</title></head>';
        verificationTemplate += '<body><div style="color: #000;">';
        verificationTemplate += '<p style="font-weight: bold;color: #000;">Forgot your password? Need to reset it?</p></div></br>';
        verificationTemplate += '<div>';
        // verificationTemplate += '<li><span>Dear CommScope Drop Craft User,</span></li>';
        // verificationTemplate += '<li><span></span></li>';
        // verificationTemplate += '<p style="margin-top:10px;font-style: italic;font-weight: bold;"><a href="' + host + '/#/changepassword/' + accesstoken + '">Click here to reset your password.</a></p>';
        verificationTemplate += '<p style="margin-top:10px;font-style: italic;font-weight: bold;"><a href="http://commscope.xpertuniversity.com/#/changepassword/' + accesstoken + '">Click here to reset your password.</a></p>';
        // verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; background-color: #DF0003; text-decoration: none;  color: #fff;font-weight: bold;">' + newpassword + '</p></li>';
        verificationTemplate += '</div><div style="background-color: #F5F5F5;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px;padding-bottom: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }

    // template that has been used for Send Mail
    function getSupervisorRegistrationMailTemplate(host, obj) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>CommScope Reviewer Registration</title></head>';
        verificationTemplate += '<body><div style="padding: 10px; color: #000;font-weight:bold;">';
        verificationTemplate += '<p>You have been invited to be a Drop Craft Reviewer.</p></div></br>';
        verificationTemplate += '<div><ul style="list-style-type:none;margin:0 auto;">';
        verificationTemplate += '<li><span>Dear CommScope Drop Craft Reviewer,</span></li>';
        verificationTemplate += '<li><span style="font-weight:bold;">Please use these credentials to login and begin reviewing submissions.</span></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; text-decoration: none;  color: #000;;font-weight: bold;">Email: ' + obj.email + '</p></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; text-decoration: none;  color: #000;;font-weight: bold;">Password: ' + obj.password + '</p></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><a style="text-decoration: none;background-color:white;padding: 10px;border-radius: 5px;color: black;font-weight: bold;" href="http://commscope.xpertuniversity.com/">Click here to login</a></span></li>';
        verificationTemplate += '</ul></div><div style="background-color: #F5F5F5;padding-bottom: 10px;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }

    // template that has been used for Send Mail
    function getSubmissionFailTemplate(host, obj) {
        var todayYear = new Date();
        var verificationTemplate = '<html><head></head>';
        verificationTemplate += '<body style="margin:10px; padding:10px;">';
        verificationTemplate += '<h6>Dear ' + obj.firstname + ' ' + obj.lastname + ',</h6>';
        verificationTemplate += '<p>The Drop Craft Activity for installing F-Connectors on RG6 Tri-Shield drop cable has been reviewed.</p>';
        verificationTemplate += '<p>The activity needs to be performed again and re-submitted.</p>';
        // verificationTemplate += '</br><a style="padding: 10px;border-radius: 5px;font-weight: bold;" href="' + host + '/images/otp.html?otp=' + obj.otp + '">Press here to re-run the activity:</a></span></p>';
        // verificationTemplate += '</br><p style="color: black;">Or, copy and paste this code <span style="text-decoration: none;  color: #000000;font-weight: bold;">' + obj.otp + '</span> to the code field in the application</p>';
        verificationTemplate += '</br><p style="color: black;">Copy this code <span style="text-decoration: none;  color: #000000;font-weight: bold;">' + obj.otp + '</span> into the code field on the app to unlock and perform the activites again.</p>';
        // verificationTemplate += '<p>Review your results:</p>';
        verificationTemplate += '<p>Review and note the comments for the results of your submission below:</p>';
        verificationTemplate += '	<table width="600" align="center">';
        for (var i = 0; i < obj.steps.length; i++) {
            verificationTemplate += '<tr>';
            verificationTemplate += '<td style="padding:5px 0px; text-align:left;"><p><u><b>' + obj.steps[i].stepname + '</b></p></u>';
            verificationTemplate += '<p><img src="' + obj.steps[i].imageUrl + '" style="width:200px;"></p>';
            verificationTemplate += '<p style="">' + ((obj.steps[i].passfail) ? 'Passed' : 'Failed') + '</p>';
            verificationTemplate += '<p  style="width:220px;word-wrap: break-word;"><b>Comments: </b><span style="resize:none;" >' + obj.steps[i].comment + '</span></p> ';
            verificationTemplate += '<p  style="width:220px;"><hr></p> ';
            // verificationTemplate += '<td></td>';
            verificationTemplate += '</td></tr>';
            // verificationTemplate += '</td></tr><tr><td><hr></td></tr>';
            // verificationTemplate += '	<tr>';
            // verificationTemplate += '<td><img src="' + obj.steps[i].imageUrl + '" style="width:200px;"></td>';
            // verificationTemplate += '	<td style="padding-left: 16px;"><p style="">' + ((obj.steps[i].passfail) ? 'Passed' : 'Failed') + '</p></br></br></br>';
            // verificationTemplate += '	<p  width: 100px;word-wrap: break-word;><b>Comments:</b><span style="margin-top:-70px; resize:none;" >' + obj.steps[i].comment + '</span></p> </td>';
            // verificationTemplate += '	</tr>';
            // verificationTemplate += '<tr><hr></tr>';
        }
        verificationTemplate += '	</table>';
        verificationTemplate += '<p><b>If you have any questions or issues, please contact your supervisor.</b></p>';
        verificationTemplate += '</body>';
        verificationTemplate += '</html>';
        return verificationTemplate;
    }

    // for (var i = 0; i < obj.steps.length; i++) {
    //     verificationTemplate += '<tr>';
    //     verificationTemplate += '<td style="padding:5px 0px; text-align:center;"><u><b>' + obj.steps[i].stepname + '</b></u></td>';
    //     verificationTemplate += '<td></td>';
    //     verificationTemplate += '</tr>';
    //     verificationTemplate += '	<tr>';
    //     verificationTemplate += '<td><img src="' + obj.steps[i].imageUrl + '" style="width:200px;"></td>';
    //     verificationTemplate += '	<td style="padding-left: 16px;"><p style="">' + ((obj.steps[i].passfail) ? 'Passed' : 'Failed') + '</p></br></br></br>';
    //     verificationTemplate += '	<p  width: 100px;word-wrap: break-word;><b>Comments:</b><span style="margin-top:-70px; resize:none;" >' + obj.steps[i].comment + '</span></p> </td>';
    //     verificationTemplate += '	</tr>';
    //     verificationTemplate += '<tr><hr></tr>';
    // }

    function getSubmissionPassTemplate(host, obj) {
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        var verificationTemplate = ' <html>';
        verificationTemplate += '<head>';
        verificationTemplate += '	<title></title>';
        verificationTemplate += '<style>';
        verificationTemplate += ' @page {';
        // verificationTemplate +=    '    size: 8.5in 11in;';
        verificationTemplate += '    size: 11in 8.5in;';
        verificationTemplate += '     margin: 0in;';
        verificationTemplate += '  }';
        // verificationTemplate += '@page { size : landscape 11in 8.5in; margin: 0in;}';
        verificationTemplate += '  html, body {';
        // verificationTemplate += '      width: 11in;';
        // verificationTemplate +=   '     height: 8.5in;';
        // verificationTemplate += '    margin: 0;';
        verificationTemplate += '    margin-left: -1px;';
        // verificationTemplate += '    margin-top: 7.5%;';
        verificationTemplate += '    font-family: "Arial", sans-serif;';
        verificationTemplate += ' }';
        // verificationTemplate += '@font-face {';
        // verificationTemplate += 'font-family: SEGOEUI;';
        // verificationTemplate += 'src: url(font/SEGOEUI.TTF);';
        // verificationTemplate += '}';
        // verificationTemplate += '@font-face {';
        // verificationTemplate += 'font-family: SEGUISLI;';
        // verificationTemplate += 'src: url(font/SEGOEUI.TTF);';
        // verificationTemplate += '}		   ';
        verificationTemplate += '</style>';
        verificationTemplate += '</head>';
        verificationTemplate += '<body style="margin:0px padding:0px;width:100%;height:100%;">';
        verificationTemplate += '<div style="margin:0 auto;text-align:center;box-shadow: 0px 0px 1px 2px #808080;">';
        verificationTemplate += '<div style="padding:10px 10px 0px 10px;">';
        verificationTemplate += '<img src="images/commscope_logo.png" style="width:100%;">';
        verificationTemplate += '</div>';
        verificationTemplate += '<p style="text-transform: uppercase; text-align:center; font-size:68px;letter-spacing: 0.3em; font-family: Segoe UI; margin:15px 0px; text-shadow: 1px 1px #b3b3b3; letter-spacing: 2px;padding:0px 10px;">Drop craft evaluation certificate of completion</p>';
        verificationTemplate += '<div style="">';
        verificationTemplate += '<p style="text-align:center; font-size:60px; font-family: Segoe UI; margin: 0px 0px 30px 0px; text-shadow: 1px 1px #b3b3b3; letter-spacing: 2px;">' + capitalize(obj.firstname) + ' ' + capitalize(obj.lastname) + '</p>';
        verificationTemplate += '</div>';
        verificationTemplate += '<div style="clear:both;"></div>			';
        verificationTemplate += '<div style="float:left; width:20%; height:320px;">';
        verificationTemplate += '<img src="images/right_pin.png" style="width:100%;height:300px;">';
        verificationTemplate += '</div>';
        verificationTemplate += '<div style="float:right;width:20%; height:320px;">';
        verificationTemplate += '<img src="images/left_pin.png" style="width:100%;height:300px;">';
        verificationTemplate += '</div>';
        verificationTemplate += '<div class="clear:both;">';
        verificationTemplate += '</div>';
        verificationTemplate += '<div style="text-align:center; font-size:42px;line-height:1em;word-spacing: 2px;margin-top:-5px; text-shadow: 1px 1px #b3b3b3; font-family:SEGUIS LI;"><i>';
        verificationTemplate += 'Has demonstrated the skills<br/>necessary to prepare and<br/>install compression-type<br/>F-connectors on coaxial<br/>drop cable';
        verificationTemplate += '</i></div>';
        verificationTemplate += '<div style="font-size:35px;">';
        verificationTemplate += '<p>' + now + '</p>';
        verificationTemplate += '</div>';
        verificationTemplate += '</div>';
        verificationTemplate += '</body>';
        verificationTemplate += '</html>';

        return verificationTemplate;
    }



    //register new supervisor
    app.post('/registeradmin', function(req, res, next) {
        if (req.body.name && req.body.email && req.body.password) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.alreadyExist(superadmin, function(err, superadmins) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmins.length > 0) {
                    res.json(error.EMAIL_ID_ALREADY_EXITS);
                } else {
                    SuperAdmin.create(superadmin, function(err, superadmin) {
                        if (err) return res.json(error.SERVER_ERROR);
                        var result = error.OK;
                        // res.json(error.EMAIL_SEND_SUCCESS);
                        result.result_data = superadmin;
                        res.json(result);
                    });
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    //superadminlogin login
    app.post('/login', function(req, res, next) {

        if (req.body.email && req.body.password) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.checkLogin(req.body, function(err, superadmin) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmin) {
                    var result = error.OK;
                    result.result_data = superadmin;
                    res.json(result);
                } else {
                    Company.find({
                        'users': {
                            $all: [{
                                $elemMatch: {
                                    'email': req.body.email,
                                    'password': req.body.password
                                }
                            }]
                        }
                    }, function(err, resu) {
                        console.log(resu);
                        if (resu.length > 0) {
                            // res.json(resu[0]._id);
                            var user = {};
                            for (var i = 0; i < resu[0].users.length; i++) {
                                if (resu[0].users[i].email == req.body.email && resu[0].users[i].password == req.body.password) {
                                    user = resu[0].users[i];
                                    break;
                                }
                            }
                            var result = error.OK;
                            result.companyid = resu[0]._id
                            result.companyname = resu[0].companyname,
                                result.result_data = user;
                            res.json(result);
                        } else {
                            res.json(error.ERROR_AUTHENTICATION);
                        }
                    });
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    // check user exist or not then send token
    app.post('/checkuserexistornot', function(req, res, next) {
        if (req.body.email) {
            Company.find({
                'users': {
                    $all: [{
                        $elemMatch: {
                            'email': req.body.email
                        }
                    }]
                }
            }, function(err, resu) {
                console.log(resu);
                if (resu.length > 0) {
                    // res.json(resu[0]._id);
                    var user = {};
                    var accessToken = generatePassword() + generatePassword() + generatePassword() + generatePassword();
                    for (var i = 0; i < resu[0].users.length; i++) {
                        if (resu[0].users[i].email == req.body.email) {
                            user = resu[0].users[i];
                            user.accessToken = accessToken;
                            user.isPasswordVerified = false;
                            user.linkvisited = false;
                            user.passwordLinkGeneratedAt = new Date().toISOString();
                            resu[0].users[i] = user;
                            break;
                        }
                    }

                    resu[0].save(function(err) {
                        if (err) {
                            console.log(err);
                            return res.json(error.SERVER_ERROR);
                        } else {
                            var mailOptions = {
                                to: req.body.email,
                                from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                subject: 'Reset Password',
                                message: getSendChangePasswordLinkTemplate(req.host, req.body, accessToken),
                                altText: 'plain text'
                            };
                            sesClient.sendEmail(mailOptions, function(err, data, resp) {
                                // ...
                                if (err) {
                                    console.log(err);
                                    res.json(error.ERROR_VARIFICATION_EMAIL);
                                } else {
                                    var result = error.OK;
                                    result.companyid = resu[0]._id
                                    // result.result_data = user;
                                    result.accesstoken = accessToken;
                                    res.json(result);
                                }
                            });

                        }
                    });
                } else {
                    res.json(error.ERROR_EMAIL_NOT_FOUND);
                }
            });
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
    });

    // checkResetPaswordExpirationProcess
    app.post('/checkResetPaswordExpirationProcess', function(req, res, next) {
        if (req.body.accesstoken) {
            Company.aggregate([{
                    "$unwind": "$users"
                },
                {
                    "$match": {
                        "users.accessToken": req.body.accesstoken
                    }
                },
                {
                    "$project": {
                        "companyname": "$companyname",
                        "useremail": "$users.email",
                        "userid": "$users._id",
                        "accessToken": "$users.accessToken",
                        "isPasswordVerified": "$users.isPasswordVerified",
                        "linkvisited": "$users.linkvisited",
                        "passwordLinkGeneratedAt": "$users.passwordLinkGeneratedAt"
                    }
                }
            ], function(err, users) {
                if (err) {
                    return res.json(error.SERVER_ERROR);
                } else {
                    if (!users.length > 0) {
                        return res.json(error.RESET_PASSWORD_LINK_EXPIRED);
                    }
                    var companyid = users[0]._id;
                    var tempUser = users[0];

                    if (users[0].linkvisited) {
                        return res.json(error.ALREADY_VISITED_RESET_PASSWORD);
                    } else if (users[0].isPasswordVerified) {
                        return res.json(error.RESET_PASSWORD_VERIFIED);
                    } else {
                        var currentTime = new Date();
                        var linkgeneratedtime = new Date(users[0].passwordLinkGeneratedAt);
                        if ((currentTime.getTime() - linkgeneratedtime.getTime()) <= (1000 * 60 * 60 * 24 * 3)) {
                            Company.findById(companyid, function(err, resu) {
                                if (resu) {
                                    var user = {};
                                    for (var i = 0; i < resu.users.length; i++) {
                                        if (resu.users[i].email == tempUser.useremail) {
                                            user = resu.users[i];
                                            user.linkvisited = true;
                                            resu.users[i] = user;
                                            resu.save(function(err) {
                                                if (err) {
                                                    return res.json(error.SERVER_ERROR)
                                                } else {
                                                    var result = error.OK;
                                                    result.companyid = companyid;
                                                    result.result_data = resu.users[i];
                                                    res.json(result);
                                                }
                                            });
                                            break;
                                        }
                                    }
                                } else {
                                    res.json(error.ERROR_AUTHENTICATION);
                                }
                            });
                            // return res.json(users[0]);
                        } else {
                            return res.json(error.RESET_PASSWORD_LINK_EXPIRED);
                        }
                    }
                }

            });
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
    });



    // change password
    app.post('/changepassword', function(req, res, next) {
        if (req.body.companyid && req.body.id && req.body.password) {
            Company.find({
                _id: req.body.companyid
            }, function(err, companies) {
                if (companies && companies.length > 0) {

                    for (var i = 0; i < companies[0].users.length; i++) {
                        if (companies[0].users[i]._id == req.body.id) {
                            companies[0].users[i].password = req.body.password;
                        }
                    }

                    companies[0].save(function(err) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        } else {
                            res.json(error.OK);
                        }
                    });
                } else {
                    return res.json(error.DATA_NOT_FOUND);
                }

            });
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
    });


    //supervisor login
    app.post('/supervisorlogin', function(req, res, next) {
        if (req.body.supervisoremailid && req.body.password) {
            var supervisor = new Supervisor(req.body);
            supervisor.checkLogin(supervisor, function(err, supervisor) {
                if (err) return res.json(error.SERVER_ERROR);
                if (supervisor) {
                    if (supervisor.isActive) {
                        var result = error.OK;
                        result.result_data = supervisor;
                        res.json(result);
                    } else {
                        res.json(error.USER_BLOCKED);
                    }
                } else {
                    res.json(error.ERROR_AUTHENTICATION);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    //supervisor login
    app.post('/supervisorlogin', function(req, res, next) {
        if (req.body.supervisoremailid && req.body.password) {
            var supervisor = new Supervisor(req.body);
            supervisor.checkLogin(supervisor, function(err, supervisor) {
                if (err) return res.json(error.SERVER_ERROR);
                if (supervisor) {
                    if (supervisor.isActive) {
                        var result = error.OK;
                        result.result_data = supervisor;
                        res.json(result);
                    } else {
                        res.json(error.USER_BLOCKED);
                    }
                } else {
                    res.json(error.ERROR_AUTHENTICATION);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    //superadmin forgot password
    app.post('/superadminforgotpassword', function(req, res, next) {
        if (req.body.superadminemailid) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.alreadyExist(superadmin, function(err, superadmins) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmins.length > 0) { //found emailid
                    var newpwd = generatePassword(); // generate new password
                    superadmins[0].changePassword(newpwd, function(err, rslt) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        }
                        var mailOptions = {
                            to: req.body.superadminemailid,
                            from: 'hitesh@mg.xpertuniversity.com',
                            subject: 'Use new Password',
                            message: getNewPasswordMailTemplate(newpwd),
                            altText: 'plain text'
                        };
                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                            // ...
                            if (err) {
                                res.json(error.ERROR_VARIFICATION_EMAIL);
                            } else {
                                res.json(error.FORGOT_EMAIL_SEND_SUCCESS);
                            }
                        });
                    });
                } else {
                    res.json(error.ERROR_EMAIL_NOT_FOUND)
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/addCompanies1', function(req, res, next) {
        var mailOptions = {
            to: req.body.email,
            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
            subject: 'Registration to CommScope',
            message: getSupervisorRegistrationMailTemplate(req.host, req.body),
            altText: 'plain text'
        };
        sesClient.sendEmail(mailOptions, function(err, data, resp) {
            if (err) {
                res.json(error.ERROR_VARIFICATION_EMAIL);
            } else {
                res.json(error.SERVER_ERROR);
            }
        });
    });

    app.post('/addCompanies', function(req, res, next) {
        if (req.body.companyname && req.body.username && req.body.email && req.body.password && req.body.isadmin && req.body.defaultreviewer) {
            var user = {};
            user.name = req.body.username;
            user.email = req.body.email;
            user.password = req.body.password;

            // add admin or reviwerrole
            if (req.body.isadmin == 'true') {
                user['role'] = "admin";
            } else {
                user['role'] = "reviewer";
            }

            // add default reviwer
            if (req.body.defaultreviewer == 'true') {
                user['defaultreviewer'] = "true";
            } else {
                user['defaultreviewer'] = "false";
            }
            if (req.body.companyid) {
                // res.json(error.SERVER_ERROR);
                var query = Company.findById(req.body.companyid);
                query.exec(function(err, company) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var includedReviewer = false;

                    if (company.users && company.users.length > 0 && req.body.isadmin != 'true') {
                        for (var i = 0; i < company.users.length; i++) {
                            // if (company.users[i].role == 'reviewer' && company.users[i].isActive) {
                            if (company.users[i].role == 'reviewer') {
                                includedReviewer = true;
                                break;
                            }
                        }
                    }

                    // add multiple reviwer in company
                    includedReviewer = false;
                    if (includedReviewer) {
                        if (req.body.defaultreviewer == 'true') {
                            // if(user['defaultreviewer'] == 'true'){
                            var alreadyExist = false;
                            for (var i = 0; i < company.users.length; i++) {
                                if (company.users[i].email == req.body.email) {
                                    alreadyExist = true;
                                }
                            }
                            if (alreadyExist) {
                                res.json(error.EMAIL_ID_ALREADY_EXITS);
                            } else {
                                for (var i = 0; i < company.users.length; i++) {
                                    // if(company.users[i].role == 'reviewer' && company.users[i].defaultreviewer == "true"){
                                    if (company.users[i].role == 'reviewer') {
                                        // company.users[i].name = req.body.username;
                                        // company.users[i].email = req.body.email;
                                        // company.users[i].password = req.body.password;
                                        company.users[i].defaultreviewer = "false";
                                        break;
                                    }
                                }
                                company.users.push(user);
                                company.save(function(err) {
                                    if (err) {
                                        res.json(error.SERVER_ERROR);
                                    } else {
                                        var mailOptions = {
                                            to: req.body.email,
                                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                            subject: 'Registration to CommScope',
                                            message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                                            altText: 'plain text'
                                        };
                                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                                            if (err) {
                                                res.json(error.ERROR_VARIFICATION_EMAIL);
                                            } else {
                                                res.json(error.OK);
                                            }
                                        });
                                    }
                                })
                            }
                        } else {
                            res.json(error.REVIWER_ALREADY_EXITS);
                        }
                    } else {
                        var isExists = false;
                        for (var i = 0; i < company.users.length; i++) {
                            if (company.users[i].email == user.email) {
                                isExists = true;
                                break;
                            }
                        }

                        if (isExists) {
                            res.json(error.EMAIL_ID_ALREADY_EXITS);
                        } else {
                            Company.update({
                                _id: req.body.companyid
                            }, {
                                $push: {
                                    users: user
                                }
                            }, function(err, result) {
                                var mailOptions = {
                                    to: req.body.email,
                                    from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                    subject: 'Registration to CommScope',
                                    message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                                    altText: 'plain text'
                                };
                                sesClient.sendEmail(mailOptions, function(err, data, resp) {
                                    if (err) {
                                        res.json(error.ERROR_VARIFICATION_EMAIL);
                                    } else {
                                        res.json(error.OK);
                                    }
                                });
                            });
                        }
                    }
                });
            } else {
                var company = new Company(req.body);
                company.users[0] = user;
                Company.create(company, function(err, company) {
                    if (err) return res.json(error.SERVER_ERROR);
                    var result = error.EMAIL_SEND_SUCCESS;
                    // res.json(error.EMAIL_SEND_SUCCESS);
                    // result.result_data = company;
                    var mailOptions = {
                        to: req.body.email,
                        from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                        subject: 'Registration to CommScope',
                        message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                        altText: 'plain text'
                    };
                    sesClient.sendEmail(mailOptions, function(err, data, resp) {
                        if (err) {
                            res.json(error.ERROR_VARIFICATION_EMAIL);
                        } else {
                            res.json(error.OK);
                        }
                    });
                });
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    //get all companies
    app.get('/getcompanies', function(req, res, next) {
        Company.find({
            isActive: true
        }, {
            _id: '_id',
            companyname: 'companyname',
            isActive: 'isActive',
            users: "users"
        }, function(err, companies) {
            if (err) {
                return res.json(error.SERVER_ERROR);
            } else {
                var responseObj = error.DATA_FOUND;
                responseObj.result_data = companies;
                res.json(responseObj);
            }
        });
    });

    //delete company
    app.post('/deletecompany', function(req, res, next) {
        if (req.body.companyid) {
            Company.remove({
                _id: req.body.companyid
            }, function(err) {
                if (err) {
                    res.json(error.SERVER_ERROR);
                } else {
                    res.json(error.OK);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/changeCompanyState', function(req, res, next) {
        if (req.body.companyid) {
            var company = new Company(req.body);
            company.alreadyExist(req.body, function(err, companies) {
                if (err) return res.json(error.SERVER_ERROR);
                if (companies.length > 0) {
                    companies[0].changeCompanyState(function(err, result) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        } else {
                            res.json(error.OK);
                        }
                    });
                } else {
                    res.json(error.DATA_NOT_FOUND)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/updateCompany', function(req, res, next) {
        if (req.body.companyid && req.body.companyname && req.body.revieweremail && req.body.reviewerid &&
            req.body.reviewername && req.body.isadmin && req.body.defaultreviewer) {
            // var company = new Company(req.body);
            Company.findById(req.body.companyid, function(err, company) {
                if (err) return res.json(error.SERVER_ERROR);
                if (company) {
                    var passwordChaged = false;
                    company.companyname = req.body.companyname;
                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i]._id == req.body.reviewerid) {
                            company.users[i].name = req.body.reviewername;
                            company.users[i].email = req.body.revieweremail;

                            if (company.users[i].password != req.body.password) {
                                company.users[i].password = req.body.password;
                                passwordChaged = true;
                            }
                            if (req.body.isadmin == 'true') {
                                company.users[i]['role'] = "admin";
                            } else {
                                company.users[i]['role'] = "reviewer";
                            }

                            // add default reviwer
                            if (req.body.defaultreviewer == 'true') {
                                company.users[i]['defaultreviewer'] = "true";
                            } else {
                                company.users[i]['defaultreviewer'] = "false";
                            }
                        } else {
                            if (req.body.defaultreviewer == 'true') {
                                company.users[i]['defaultreviewer'] = 'false';
                            }
                        }
                    }

                    if (passwordChaged) {
                        var mailOptions = {
                            to: req.body.revieweremail,
                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                            subject: 'New Password for CommScope',
                            message: getNewPasswordMailTemplate(req.body.password),
                            altText: 'plain text'
                        };
                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                            if (err) {
                                res.json(error.ERROR_VARIFICATION_EMAIL);
                            } else {
                                company.save(function(err) {
                                    if (err) {
                                        res.json(error.SERVER_ERROR);
                                    } else {
                                        res.json(error.OK);
                                    }
                                })
                            }
                        });
                    } else {
                        company.save(function(err) {
                            if (err) {
                                res.json(error.SERVER_ERROR);
                            } else {
                                res.json(error.OK);
                            }
                        })
                    }

                } else {
                    res.json(error.DATA_NOT_FOUND);
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });


    // sendResetpasswordMail
    app.post('/sendresetpasswordmail', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid && req.body.revieweremail) {
            var mailOptions = {
                to: req.body.revieweremail,
                from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                subject: 'Reset Password - CommScope',
                message: getSendResetPasswordMailTemplate(req.host, req.body),
                altText: 'plain text'
            };
            sesClient.sendEmail(mailOptions, function(err, data, resp) {
                if (err) {
                    res.json(error.WENT_WRONG_MAIL);
                } else {
                    res.json(error.OK);
                }
            });
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/getreviewer', function(req, res, next) {
        if (req.body.all) {
            if (req.body.companyid && req.body.all == 'false') {
                var company = new Company(req.body);
                company.alreadyExist(req.body, function(err, companies) {
                    if (err) return res.json(error.SERVER_ERROR);
                    if (companies[0].users.length > 0) {
                        // var reviewer = {};
                        // var found = false;
                        // for (var i = 0; i < companies[0].users.length; i++) {
                        //     if (companies[0].users[i].role == 'reviewer') {
                        //         reviewer = companies[0].users[i];
                        //         found = true;
                        //         break;
                        //     }
                        // }
                        //
                        // if (found) {
                        //     var responseObj = error.OK;
                        //     responseObj.result_data = {};
                        //     responseObj.result_data = reviewer;
                        //     res.json(responseObj);
                        // } else {
                        //     res.json(error.DATA_NOT_FOUND);
                        // }
                        var responseObj = error.OK;
                        responseObj.result_data = [];
                        responseObj.result_data = companies[0].users;
                        res.json(responseObj);
                    } else {
                        res.json(error.DATA_NOT_FOUND)
                    }
                });

            } else {
                Company.aggregate([{
                        "$unwind": "$users"
                    },
                    {
                        "$match": {
                            "users.role": "reviewer"
                        }
                    },
                    {
                        "$project": {
                            "reviewerid": "$users._id",
                            "companyname": "$companyname",
                            "reviewername": "$users.name",
                            "revieweremail": "$users.email",
                            "password": "$users.password",
                            "isActive": "$users.isActive",
                            "createdat": {
                                $dateToString: {
                                    format: "%d/%m/%Y",
                                    date: "$users.createdat"
                                }
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                })
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/updatereviewerdetail', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid && req.body.name && req.body.email && req.body.isadmin && req.body.defaultreviewer) {
            Company.findById(req.body.companyid, function(err, company) {
                if (err) {
                    res.json(error.SERVER_ERROR)
                } else {
                    var passwordChaged = false;
                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i]._id == req.body.reviewerid) {
                            company.users[i].name = req.body.name;
                            company.users[i].email = req.body.email;
                            if (req.body.isadmin == 'true') {
                                company.users[i]['role'] = "admin";
                            } else {
                                company.users[i]['role'] = "reviewer";
                            }

                            if (company.users[i].password != req.body.password) {
                                company.users[i].password = req.body.password;
                                passwordChaged = true;
                            }

                            // add default reviwer
                            if (req.body.defaultreviewer == 'true') {
                                company.users[i]['defaultreviewer'] = "true";
                            } else {
                                company.users[i]['defaultreviewer'] = "false";
                            }
                        } else {
                            if (req.body.defaultreviewer == 'true') {
                                company.users[i]['defaultreviewer'] = 'false';
                            }
                        }
                    }

                    if (passwordChaged) {
                        var mailOptions = {
                            to: req.body.email,
                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                            subject: 'New Password for CommScope',
                            message: getNewPasswordMailTemplate(req.body.password),
                            altText: 'plain text'
                        };
                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                            if (err) {
                                res.json(error.ERROR_VARIFICATION_EMAIL);
                            } else {
                                company.save(function(err) {
                                    if (err) {
                                        res.json(error.SERVER_ERROR);
                                    } else {
                                        res.json(error.OK);
                                    }
                                })
                            }
                        });
                    } else {
                        company.save(function(err) {
                            if (err) {
                                return res.json(error.SERVER_ERROR);
                            } else {
                                res.json(error.OK);
                            }
                        })
                    }

                    // res.json(company)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    })

    app.post('/updatereviewerstate', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid) {
            Company.findById(req.body.companyid, function(err, company) {
                if (err) {
                    res.json(error.SERVER_ERROR)
                } else {

                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i]._id == req.body.reviewerid) {
                            company.users[i].isActive = !company.users[i].isActive;
                            company.save(function(err) {
                                res.json(error.OK);
                            })
                            break;
                        }
                    }
                    // res.json(company)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/deletereviewer', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid) {
            Company.findById(req.body.companyid, function(err, company) {
                if (err) {
                    res.json(error.SERVER_ERROR)
                } else {

                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i]._id == req.body.reviewerid) {
                            company.users.splice(i, 1);
                            company.save(function(err) {
                                if (err) {
                                    return res.json(error.SERVER_ERROR);
                                }
                                res.json(error.OK);
                            })
                            break;
                        }
                    }
                    // res.json(company)
                }
            });
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
    })

    app.post('/getsubmissions', function(req, res, next) {
        if (req.body.all) {
            if (req.body.companyid && req.body.all == 'false') {
                User.aggregate([{
                        $lookup: {
                            from: "companies",
                            localField: "companyid",
                            foreignField: "_id",
                            as: "company"
                        }
                    },
                    {
                        $sort: {
                            submittedat: -1
                        }
                    },
                    {
                        "$match": {
                            "companyid": new mongoose.Types.ObjectId(req.body.companyid)
                        }
                    },
                    {
                        "$project": {
                            "isActive": "$isActive",
                            "verified": "$verified",
                            "isChecked": "$isChecked",
                            "emailaddress": "$emailaddress",
                            "isArchived": "$isArchived",
                            "lastname": "$lastname",
                            "firstname": "$firstname",
                            "companyid": "$companyid",
                            "companyname": "$company.companyname",
                            "company": "$company",
                            "createdat": {
                                $dateToString: {
                                    format: "%m/%d/%Y %H:%M:%S",
                                    date: "$createdat"
                                }
                            },
                            "date": {
                                $dateToString: {
                                    format: "%m/%d/%Y",
                                    date: "$createdat"
                                }
                            },
                            "stepslength": {
                                $size: "$steps"
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                });

            } else {
                User.aggregate([{
                        $lookup: {
                            from: "companies",
                            localField: "companyid",
                            foreignField: "_id",
                            as: "company"
                        }
                    },
                    {
                        $sort: {
                            submittedat: -1
                        }
                    },
                    {
                        "$project": {
                            "isActive": "$isActive",
                            "verified": "$verified",
                            "isChecked": "$isChecked",
                            "isArchived": "$isArchived",
                            "emailaddress": "$emailaddress",
                            "lastname": "$lastname",
                            "firstname": "$firstname",
                            "companyid": "$companyid",
                            "companyname": "$company.companyname",
                            "company": "$company",
                            "createdat": {
                                $dateToString: {
                                    format: "%m/%d/%Y %H:%M:%S",
                                    date: "$createdat"
                                }
                            },
                            "date": {
                                $dateToString: {
                                    format: "%m/%d/%Y",
                                    date: "$createdat"
                                }
                            },
                            "stepslength": {
                                $size: "$steps"
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                });
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/setasarchived', function(req, res, next) {
        if (req.body.userid) {
            User.findById({
                _id: req.body.userid
            }, function(err, user) {
                if (err) {
                    res.json(error.SERVER_ERROR);
                }
                if(user){
                    user.isArchived = !user.isArchived;
                    user.save(function(err){
                      if(err){
                        return res.json(error.SERVER_ERROR);
                      }else {
                        return res.json(error.OK);
                      }
                    });
                }else {
                    res.json(error.DATA_NOT_FOUND);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/getsubmissionsbyuser', function(req, res, next) {
        if (req.body.userid) {
            User.find({
                _id: req.body.userid
            }, function(err, result) {
                if (err) {
                    res.json(error.SERVER_ERROR);
                }
                if (result.length > 0) {
                    var responseObj = error.OK;
                    responseObj.result_data = result[0];
                    res.json(responseObj);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/submitsubmission', function(req, res, next) {
        if (req.body.userid && req.body.companyid && req.body.companyname && req.body.firstname && req.body.lastname &&
            req.body.steps) {
            User.findById(req.body.userid, function(err, user) {
                user.isChecked = true;
                var verified = true;
                for (var i = 0; i < req.body.steps.length; i++) {
                    user.steps[i] = req.body.steps[i];
                    if (verified && !req.body.steps[i].passfail) {
                        verified = false;
                    }
                }
                // verified means pass
                user.verified = verified;
                user.updatedat = new Date().toISOString();
                if (verified) {
                    user.passattempts = user.passattempts + 1;
                    htmlToPdf.convertHTMLString(getSubmissionPassTemplate(req.host, req.body), 'pdfs/' + user._id + '.pdf',
                        function(err, success) {
                            if (err) {
                                console.log('Oh noes! Errorz!');
                                console.log(error);
                                res.json(error.SERVER_ERROR);

                            } else {
                                console.log(user.emailaddress);
                                fs.readFile('pdfs/' + user._id + '.pdf', function(err, data) {
                                    if (err) {
                                        throw err;
                                    }
                                    var base64data = new Buffer(data, 'binary').toString('base64');
                                    var s3 = new AWS.S3();
                                    s3.putObject({
                                        Bucket: 'commspecmedia',
                                        Key: 'pdfs/' + user._id + '.pdf',
                                        Body: base64data
                                    }, function(resp) {
                                        console.log('Successfully uploaded package.');
                                        //  fs.unlink('./www/testpdf.pdf');
                                        var CRLF = '\r\n',
                                            ses = require('node-ses'),
                                            client = ses.createClient({
                                                key: 'AKIAIV4Q42UG5E2GH2IQ',
                                                secret: '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'
                                            }),
                                            rawMessage = [
                                                'From: "CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                                'To: ' + user.emailaddress + '',
                                                'Subject: Result for CommScope Submission',
                                                'Content-Type: multipart/mixed;',
                                                '    boundary="_003_97DCB304C5294779BEBCFC8357FCC4D2"',
                                                'MIME-Version: 1.0',
                                                '',
                                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                                'Content-Type: text/plain; charset="us-ascii"',
                                                'Content-Transfer-Encoding: quoted-printable',
                                                'Dear ' + user.firstname + ' ' + user.lastname + ',',
                                                '',
                                                'Congratulations. Your Submitted CommScope Drop Craft Evaluation has passed inspection.',
                                                '',
                                                'Your Certificate of Completion is attached for you to download and print.',
                                                '',
                                                'If you have any questions, please contact your supervisor.',
                                                '',
                                                'Good job.',
                                                '',
                                                'The CommScope Training Team.',
                                                '',
                                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                                'Content-Type: application/pdf; name="code.pdf"',
                                                'Content-Description: pdfs/testpdf.pdf',
                                                'Content-Disposition: attachment; filename="drop_craft_cert.pdf"; size=4;',
                                                '    creation-date="Mon, 03 Aug 2015 11:39:39 GMT";',
                                                '    modification-date="Mon, 03 Aug 2015 11:39:39 GMT"',
                                                'Content-Transfer-Encoding: base64',
                                                '',
                                                '' + new Buffer(data, 'binary').toString('base64') + '',
                                                ''
                                            ].join(CRLF);

                                        // var base64data = new Buffer(data, 'binary').toString('base64');
                                        sesClient.sendRawEmail({
                                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                            rawMessage: rawMessage
                                        }, function(err, data, resp) {
                                            // ...
                                            fs.unlink('pdfs/' + user._id + '.pdf');
                                            console.log("pass send mail");
                                            user.pdfUrl = 'https://s3.amazonaws.com/commspecmedia/pdfs/'+ user._id + '.pdf';
                                            user.save(function(err) {
                                                if (err) {
                                                    res.json(error.SERVER_ERROR);
                                                } else {
                                                    // res.json(error.OK);
                                                    console.log("user.save");
                                                    Result.find({
                                                        userid: user._id,
                                                        companyid: user.companyid,
                                                        emailaddress: user.emailaddress
                                                    }, function(err, results) {
                                                        if (results.length > 0) {
                                                            if (verified) {
                                                                results[0].passattempts = results[0].passattempts + 1;
                                                            } else {
                                                                results[0].failattempts = results[0].failattempts + 1;
                                                            }
                                                            console.log("user.save if");
                                                            results[0].updatedat = new Date().toISOString();
                                                            results[0].attempts = results[0].attempts + 1;
                                                            results[0].save(function(err) {
                                                                if (err) {
                                                                    return res.json(error.SERVER_ERROR);
                                                                } else {
                                                                    return res.json(error.OK);
                                                                }
                                                            });
                                                        } else {
                                                            console.log("user.save else");
                                                            var tempresult = {};
                                                            tempresult.userid = user._id;
                                                            tempresult.companyid = user.companyid,
                                                                tempresult.emailaddress = user.emailaddress;
                                                            if (verified) {
                                                                tempresult.passattempts = 1;
                                                            } else {
                                                                tempresult.failattempts = 1;
                                                            }
                                                            tempresult.attempts = 1;
                                                            var result = new Result(tempresult);
                                                            Result.create(result, function(err, result) {
                                                                if (err) res.json(error.SERVER_ERROR);
                                                                return res.json(error.OK);
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                    });

                                });
                            }
                        });
                } else {
                    user.failattempts = user.failattempts + 1;
                    user.isActive = false;
                    user.otp = generatePassword();
                    req.body.otp = user.otp;
                    // console.log("1:"+req.body.otp);
                    // console.log("2:"+user.otp);
                    var mailOptions = {
                        // to: user.email,
                        to: user.emailaddress,
                        from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                        subject: 'Result for CommScope Submission',
                        message: getSubmissionFailTemplate(req.host, req.body),
                        altText: 'plain text'
                    };
                    sesClient.sendEmail(mailOptions, function(err, data, resp) {
                        if (err) {
                            res.json(error.ERROR_VARIFICATION_EMAIL);
                        } else {
                            // console.log(user);
                            console.log("fail send mail");
                            user.save(function(err) {
                                if (err) {
                                    res.json(error.SERVER_ERROR);
                                } else {
                                    // res.json(error.OK);
                                    console.log("user.save ");
                                    Result.find({
                                        userid: user._id,
                                        companyid: user.companyid,
                                        emailaddress: user.emailaddress
                                    }, function(err, results) {
                                        if (results.length > 0) {
                                            if (verified) {
                                                results[0].passattempts = results[0].passattempts + 1;
                                            } else {
                                                results[0].failattempts = results[0].failattempts + 1;
                                            }
                                            results[0].updatedat = new Date().toISOString();
                                            results[0].attempts = results[0].attempts + 1;
                                            results[0].save(function(err) {
                                                if (err) {
                                                    return res.json(error.SERVER_ERROR);
                                                } else {
                                                    console.log("user.save if");
                                                    return res.json(error.OK);
                                                }
                                            });
                                        } else {
                                            var tempresult = {};
                                            tempresult.userid = user._id;
                                            tempresult.companyid = user.companyid,
                                                tempresult.emailaddress = user.emailaddress;
                                            if (verified) {
                                                tempresult.passattempts = 1;
                                            } else {
                                                tempresult.failattempts = 1;
                                            }
                                            tempresult.attempts = 1;
                                            var result = new Result(tempresult);
                                            Result.create(result, function(err, result) {
                                                if (err) {
                                                    return res.json(error.SERVER_ERROR);
                                                } else {
                                                    console.log("user.save else");
                                                    return res.json(error.OK);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            })
        }
    });

    app.get('/getdashboardstats', function(req, res, next) {
        var passed = 0;
        var failedResubmitted = 0;
        var failedResubmittedNot48 = 0;
        var failedResubmittedNot30 = 0;
        Result.aggregate([{
            $group: {
                _id: null,
                passed: {
                    $sum: "$passattempts"
                }
            }
        }], function(err, result) {
            if (result.length > 0) {
                passed = result[0].passed;
            }
            User.find({
                $where: "this.steps.length > 0 && this.attempts == 1"
            }, function(err, users) {
                if (users.length > 0) {
                    failedResubmitted = users.length;
                }

                User.find({
                    $where: function() {
                        return (Date.now() - this.createdat < (48 * 60 * 60 * 1000) && (this.isChecked == true) && (this.verified == false))
                    }
                }, function(err, users) {
                    if (err) {
                        return res.json(error.SERVER_ERROR);
                    } else {
                        if (users.length > 0) {
                            var tempusers = users;
                            User.find({
                                $where: function() {
                                    return (Date.now() - this.createdat < (48 * 60 * 60 * 1000) && (this.attempts == 1))
                                }
                            }, function(err, users) {
                                var result = error.SERVER_ERROR;
                                for (var i = 0; i < tempusers.length; i++) {
                                    for (var j = 0; j < users.length; j++) {
                                        if (tempusers[i].emailaddress == users.emailaddress && tempusers[i].companyid == users.companyid && !users.isChecked) {
                                            tempusers.splice(i, 1);
                                            break;
                                        }
                                    }
                                }

                                failedResubmittedNot48 = tempusers.length;
                                var today = new Date();
                                var objDate = new Date();
                                var count = 0;
                                for (var i = 0; i < tempusers.length; i++) {
                                    objDate = new Date(tempusers[i].createdat)
                                    if (((today.getTime() - objDate.getTime()) <= (1000 * 60 * 60 * 30))) {
                                        count++;
                                    }
                                }
                                failedResubmittedNot30 = count;
                                var resObj = {
                                    "passed": passed,
                                    "failedResubmitted": failedResubmitted,
                                    "failedResubmittedNot48": failedResubmittedNot48,
                                    "failedResubmittedNot30": failedResubmittedNot30
                                }
                                result.result_data = resObj;
                                return res.json(result);
                            });
                        } else {
                            var result = error.OK;
                            var resObj = {
                                "passed": passed,
                                "failedResubmitted": failedResubmitted,
                                "failedResubmittedNot48": failedResubmittedNot48,
                                "failedResubmittedNot30": failedResubmittedNot30
                            }
                            result.result_data = resObj;
                            return res.json(result);
                        }
                    }
                })

            });
            // res.json(result[0].passed);
        });
    });

    //old flow
    app.get('/getstatistics', function(req, res, next) {

        User.aggregate([{
            "$project": {
                "_id": 0,
                "verified": "$verified",
                "isChecked": "$isChecked",
                "createdat": "$createdat",
                "passattempts": "$passattempts",
                "failattempts": "$failattempts",
                "stepslength": {
                    $size: "$steps"
                },
                "attempts": "$attempts"
            }
        }], function(err, result) {
            if (err) {
                res.json(error.SERVER_ERROR);
            }
            var responseObj = error.OK;
            responseObj.result_data = result;
            res.json(responseObj);
        });
    });

    //get dash info flow(new 02022017)
    app.post('/getdashstatistics', function(req, res, next) {
        if (req.body.all) {
            if (req.body.companyid && req.body.all == 'false') {
                User.find({
                    $where: 'this.steps.length>0',
                    companyid: req.body.companyid
                }, {
                    "verified": "$verified",
                    "isChecked": "$isChecked",
                    "submittedat": "$submittedat",
                    "passattempts": "$passattempts",
                    "failattempts": "$failattempts",
                    "attempts": "$attempts"
                }, function(err, users) {
                    if (err) {
                        console.log(err);
                        res.json(error.SERVER_ERROR);
                    }
                    for (var i = 0; i < users.length; i++) {
                        if (!users[i].isChecked) {
                            pendingreview++;
                        } else {
                            if (users[i].verified) {
                                passed++;
                            } else {
                                failed++;
                            }
                        }
                    }
                    var result = error.OK;
                    var resObj = {
                        "passed": passed,
                        "failed": failed,
                        "pendingreview": pendingreview
                    }
                    // responseObj.result_data = users;

                    result.result_data = resObj;
                    return res.json(result);
                });
            } else {
                User.find({
                    $where: 'this.steps.length>0'
                }, {
                    "verified": "$verified",
                    "isChecked": "$isChecked",
                    "submittedat": "$submittedat",
                    "passattempts": "$passattempts",
                    "failattempts": "$failattempts",
                    "attempts": "$attempts"
                }, function(err, users) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }

                    for (var i = 0; i < users.length; i++) {
                        if (!users[i].isChecked) {
                            pendingreview++;
                        } else {
                            if (users[i].verified) {
                                passed++;
                            } else {
                                failed++;
                            }
                        }
                    }
                    var result = error.OK;
                    var resObj = {
                        "passed": passed,
                        "failed": failed,
                        "pendingreview": pendingreview
                    }
                    // responseObj.result_data = users;

                    result.result_data = resObj;
                    return res.json(result);
                });
            }
        } else {
            return res.json(error.MANDATORY_FIELDS);
        }
        var passed = 0;
        var pendingreview = 0;
        var failed = 0;

    });

    // upload pdf demo
    app.get('/uploadpdf', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        fs.readFile('pdfs/test.pdf', function(err, data) {
            if (err) {
                throw err;
            }

            var base64data = new Buffer(data, 'binary').toString('base64');
            var s3 = new AWS.S3();
            s3.putObject({
                Bucket: 'commspecmedia',
                Key: 'pdfs/testpdf.pdf',
                Body: base64data,
                ContentType : 'application/pdf'
            }, function(resp) {
                console.log('Successfully uploaded package.');
                //  fs.unlink('./www/testpdf.pdf');
                return res.json(error.OK);
            });

        });
    });

    app.get('/createpdf', function(req, res, next) {
        req.body.firstname = "pradip";
        req.body.lastname = "kachhadiya"
        htmlToPdf.convertHTMLString(getSubmissionPassTemplate(req.host, req.body), 'pdfs/testpdf.pdf',
            function(err, success) {
                if (err) {
                    console.log('Oh noes! Errorz!');
                    console.log(error);
                    res.json(error.SERVER_ERROR);

                } else {
                    fs.readFile('pdfs/testpdf.pdf', function(err, data) {
                        // res.json(data);

                        var CRLF = '\r\n',
                            ses = require('node-ses'),
                            client = ses.createClient({
                                key: 'AKIAIV4Q42UG5E2GH2IQ',
                                secret: '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'
                            }),
                            rawMessage = [
                                'From: "CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                'To: pradipkachhadiya.ibl@gmail.com',
                                'Subject: greetings',
                                'Content-Type: multipart/mixed;',
                                '    boundary="_003_97DCB304C5294779BEBCFC8357FCC4D2"',
                                'MIME-Version: 1.0',
                                '',
                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                'Content-Type: text/plain; charset="us-ascii"',
                                'Content-Transfer-Encoding: quoted-printable',
                                'Dear ' + req.body.firstname + ' ' + req.body.lastname + ',',
                                '',
                                'Congratulations. Your Submitted CommScope Drop Craft Evaluation has passed inspection.',
                                '',
                                'Your Certificate of Completion is attached for you to download and print.',
                                '',
                                'If you have any questions, please contact your supervisor.',
                                '',
                                'Good job.',
                                '',
                                'The CommScope Training Team.',
                                '',
                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                'Content-Type: application/pdf; name="code.pdf"',
                                'Content-Description: pdfs/testpdf.pdf',
                                'Content-Disposition: attachment; filename="drop_craft_cert.pdf"; size=4;',
                                '    creation-date="Mon, 03 Aug 2015 11:39:39 GMT";',
                                '    modification-date="Mon, 03 Aug 2015 11:39:39 GMT"',
                                'Content-Transfer-Encoding: base64',
                                '',
                                '' + new Buffer(data, 'binary').toString('base64') + '',
                                ''
                            ].join(CRLF);

                        // var base64data = new Buffer(data, 'binary').toString('base64');
                        sesClient.sendRawEmail({
                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                            rawMessage: rawMessage
                        }, function(err, data, resp) {
                            // ...
                            res.json(error.OK)
                        });
                    });
                    // res.json(error.OK);

                }
            }
        );
        // var PDF = require('pdfkit'); //including the pdfkit module
        // var fs = require('fs');
        // var text = 'ANY_TEXT_YOU_WANT_TO_WRITE_IN_PDF_DOC';
        // var filename = "testpdf";
        // doc = new PDF(); //creating a new PDF object
        // doc.pipe(fs.createWriteStream('pdfs/' + filename + '.pdf')); //creating a write stream
        // doc.image('images/ic_comscope_big.png', 150, 150);
        // doc.text(text, 150, 100); //adding the text to be written,
        // // more things can be added here including new pages
        // doc.end();

    });

    app.get('/dateformat', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        res.json(now);
    });

    app.get('/dateformat', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        res.json(now);
    });

    app.get('/updateResult', function(req, res, next) {
        var userid = "5885919ea696500a201ba69e";
        var companyid = "58830988b9c5f808fc307ec4";
        var emailaddress = "pradipkachhadiya.ibl@gmail.com";
        var pass = true;
        Result.find({
            userid: userid,
            companyid: companyid,
            emailaddress: emailaddress
        }, function(err, results) {
            if (results.length > 0) {
                if (pass) {
                    results[0].passattempts = results[0].passattempts + 1;
                } else {
                    results[0].failattempts = results[0].failattempts + 1;
                }
                results[0].updatedat = new Date().toISOString();
                // results[0].attempts = results[0].attempts + 1;
                results[0].save(function(err) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    } else {
                        res.json(error.OK);
                    }
                });
            } else {
                var tempresult = {};
                tempresult.userid = userid;
                tempresult.companyid = companyid,
                    tempresult.emailaddress = emailaddress;
                var result = new Result(tempresult);
                Result.create(result, function(err, result) {
                    if (err) res.json(error.SERVER_ERROR);
                    res.json(result);
                });
            }
        });
    });

};
