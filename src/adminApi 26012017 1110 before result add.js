var async = require("async");
var s3fs = require("s3fs");
var PDFDocument = require('pdfkit'); // add pdfkit module to access it
var htmlToPdf = require('html-to-pdf');
var AWS = require('aws-sdk'),
    fs = require('fs');
AWS.config.update({
    accessKeyId: 'AKIAIZYVRLJ3VCHSRYZA',
    secretAccessKey: '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'
});
module.exports = function(app, mongoose, nodemailerutil, ses, sesClient, passport, jwt, crypto, fs, error) {
    require('../models/Company');
    require('../models/Step');
    require('../models/Tutorial');
    require('../models/Supervisor');
    require('../models/SuperAdmin');
    require('../models/User');
    require('../models/Result');

    var Company = mongoose.model('Company');
    var Step = mongoose.model('Step');
    var Tutorial = mongoose.model('Tutorial');
    var Supervisor = mongoose.model('Supervisor');
    var SuperAdmin = mongoose.model('SuperAdmin');
    var User = mongoose.model('User');
    var Result = mongoose.model('Result');

    function generatePassword() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4();
    }

    // template that has been used for Send Mail
    function getNewPasswordMailTemplate(newpassword) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>New Password</title></head>';
        verificationTemplate += '<body><div style="background-color: #DF0003; padding: 10px; color: #fff;">';
        verificationTemplate += '<p>Accepted New Password Response by CommScope team</p></div></br>';
        verificationTemplate += '<div><ul style="list-style-type:none;margin:0 auto;">';
        verificationTemplate += '<li><span>Dear CommScope user,</span></li>';
        verificationTemplate += '<li><span>Please use this new password for login.</span></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; background-color: #DF0003; text-decoration: none;  color: #fff;font-weight: bold;">' + newpassword + '</p></li>';
        verificationTemplate += '</ul></div><div style="background-color: #F5F5F5;min-height:50px;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }

    // template that has been used for Send Mail
    function getSupervisorRegistrationMailTemplate(host, obj) {
        var todayYear = new Date();
        var verificationTemplate = '<!DOCTYPE html><html><head>';
        verificationTemplate += '<meta name="viewport" content="width=device-width" />';
        verificationTemplate += '<title>CommScope Registration</title></head>';
        verificationTemplate += '<body><div style="padding: 10px; color: #000;font-weight:bold;">';
        verificationTemplate += '<p>You are invited as a supervisor by CommScope Team.</p></div></br>';
        verificationTemplate += '<div><ul style="list-style-type:none;margin:0 auto;">';
        verificationTemplate += '<li><span>Dear CommScope supervisor,</span></li>';
        verificationTemplate += '<li><span style="font-weight:bold;">Please use this credentils for login.</span></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; text-decoration: none;  color: #000;;font-weight: bold;">Email: ' + obj.email + '</p></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><p style="padding: 10px; text-decoration: none;  color: #000;;font-weight: bold;">Password: ' + obj.password + '</p></li>';
        verificationTemplate += '<li style="margin-top: 20px;"><a style="text-decoration: none;background-color:white;padding: 10px;border-radius: 5px;color: black;font-weight: bold;" href="' + host + '/#/login">Click Here to login</a></span></li>';
        verificationTemplate += '</ul></div><div style="background-color: #F5F5F5;min-height:50px;text-align:center;">';
        verificationTemplate += '<p style="padding-top: 10px; color: #999999;">Copyright © ' + todayYear.getFullYear() + ' CommScope - All Rights Reserved</p></div>';
        verificationTemplate += '</body></html>';
        return verificationTemplate;
    }

    // template that has been used for Send Mail
    function getSubmissionFailTemplate(host, obj) {
        console.log("------------" + obj.otp);
        var todayYear = new Date();
        var verificationTemplate = '<html><head></head>';
        verificationTemplate += '<body style="margin:0; padding:0;">';
        verificationTemplate += '<h6>Dear ' + obj.firstname + ' ' + obj.lastname + ',</h6>';
        verificationTemplate += '<p>The Drop Craft Activity for installing F-Connectors on RG6 Tri-Shield drop cable has been reviewed.</p>';
        verificationTemplate += '<p>The activity needs to be performed again and re-submitted.</p>';
        verificationTemplate += '</br><a style="padding: 10px;border-radius: 5px;font-weight: bold;" href="' + host + '/images/otp.html?otp=' + obj.otp + '">Press here to re-run the activity:</a></span></p>';
        verificationTemplate += '</br><p style="color: black;">Or, copy and paste this code <span style="text-decoration: none;  color: #000000;font-weight: bold;">' + obj.otp + '</span> to the code field in the application</p>';
        verificationTemplate += '<p>Review your results:</p>';
        verificationTemplate += '	<table width="600" align="center">';
        for (var i = 0; i < obj.steps.length; i++) {
            verificationTemplate += '<tr>';
            verificationTemplate += '<td style="padding:5px 0px; text-align:center;"><u><b>' + obj.steps[i].stepname + '</b></u></td>';
            verificationTemplate += '<td></td>';
            verificationTemplate += '</tr>';
            verificationTemplate += '	<tr>';
            verificationTemplate += '<td><img src="' + obj.steps[i].imageUrl + '" style="width:200px;"></td>';
            verificationTemplate += '	<td style="margin-top:20px; padding-left: 16px;"><p style="margin-left:21px; ">' + ((obj.steps[i].passfail) ? 'Passed' : 'Failed') + '</p></br></br></br>';
            verificationTemplate += '	<p><b>Comments :</b></p><p style="margin-top:-70px; margin-left:110px; resize:none;" >' + obj.steps[i].comment + '</p> </td>';
            verificationTemplate += '	</tr>';
        }
        verificationTemplate += '	</table>';
        verificationTemplate += '<b>If you have any questions or issues, please contact your supervisor.</b>';
        verificationTemplate += '</body>';
        verificationTemplate += '</html>';
        return verificationTemplate;
    }

    // template that has been used for Send Mail
    function getSubmissionPassTemplate(host, obj) {
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        var verificationTemplate = '<html><head></head>';
        verificationTemplate += '  <body style="margin:0px padding:0px;">';
        verificationTemplate += '	<div style="width:595px;margin:0 auto;text-align:center;box-shadow: 0px 0px 1px 2px #808080;padding: 30px;">';
        verificationTemplate += '		<div>';
        verificationTemplate += '		<img src="images/ic_comscope@3x.png" style="width:100%;">';
        verificationTemplate += '		</div>';
        verificationTemplate += '		<p style="text-transform: uppercase; text-align:center; font-size:26px;  text-shadow: 1px 1px #b3b3b3;">drop craft evaluation certificate of completion</p>';
        verificationTemplate += '		<div style="float:left;width:80px;">';
        verificationTemplate += '			<img src="images/leftpin.png" style="width:100%;">';
        verificationTemplate += '		</div>';
        verificationTemplate += '		<div style="float:right;width:80px;">';
        verificationTemplate += '			<img src="images/rightpin.png" style="width:100%;">';
        verificationTemplate += '		</div>';
        verificationTemplate += '		<div style="">';
        verificationTemplate += '			<p style="text-transform: uppercase; text-align:center; font-size:26px;  text-shadow: 1px 1px #b3b3b3;margin-top: 50px;">' + obj.firstname + ' ' + obj.lastname + '</p>';
        verificationTemplate += '		</div>';
        verificationTemplate += '		<div style="clear:both;"></div>';
        verificationTemplate += '  		<div style="text-align:center; font-size:20px;  text-shadow: 1px 1px #b3b3b3;margin-top:50px;">';
        verificationTemplate += '			Has Successfully completed an evaluation of the skills needed to properly prepare and install F-connector on coaxial cable';
        verificationTemplate += '  		</div>';
        verificationTemplate += '  		<div style="font-size:25px;">';
        verificationTemplate += '  			<p>' + now + '</p>';
        verificationTemplate += '  		</div>';
        verificationTemplate += '		</div>';
        verificationTemplate += '  </body>';
        verificationTemplate += '</html>';
        return verificationTemplate;
    }



    //register new supervisor
    app.post('/registeradmin', function(req, res, next) {
        if (req.body.name && req.body.email && req.body.password) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.alreadyExist(superadmin, function(err, superadmins) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmins.length > 0) {
                    res.json(error.EMAIL_ID_ALREADY_EXITS);
                } else {
                    SuperAdmin.create(superadmin, function(err, superadmin) {
                        if (err) return res.json(error.SERVER_ERROR);
                        var result = error.OK;
                        // res.json(error.EMAIL_SEND_SUCCESS);
                        result.result_data = superadmin;
                        res.json(result);
                    });
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    //superadminlogin login
    app.post('/login', function(req, res, next) {

        if (req.body.email && req.body.password) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.checkLogin(req.body, function(err, superadmin) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmin) {
                    var result = error.OK;
                    result.result_data = superadmin;
                    res.json(result);
                } else {
                    Company.find({
                        'users': {
                            $all: [{
                                $elemMatch: {
                                    'email': req.body.email,
                                    'password': req.body.password
                                }
                            }]
                        }
                    }, function(err, resu) {
                        console.log(resu);
                        if (resu.length > 0) {
                            // res.json(resu[0]._id);
                            var user = {};
                            for (var i = 0; i < resu[0].users.length; i++) {
                                if (resu[0].users[i].email == req.body.email && resu[0].users[i].password == req.body.password) {
                                    user = resu[0].users[i];
                                    break;
                                }
                            }
                            var result = error.OK;
                            result.companyid = resu[0]._id
                            result.result_data = user;
                            res.json(result);
                        } else {
                            res.json(error.ERROR_AUTHENTICATION);
                        }
                    });
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    //supervisor login
    app.post('/supervisorlogin', function(req, res, next) {
        if (req.body.supervisoremailid && req.body.password) {
            var supervisor = new Supervisor(req.body);
            supervisor.checkLogin(supervisor, function(err, supervisor) {
                if (err) return res.json(error.SERVER_ERROR);
                if (supervisor) {
                    if (supervisor.isActive) {
                        var result = error.OK;
                        result.result_data = supervisor;
                        res.json(result);
                    } else {
                        res.json(error.USER_BLOCKED);
                    }
                } else {
                    res.json(error.ERROR_AUTHENTICATION);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    //supervisor login
    app.post('/supervisorlogin', function(req, res, next) {
        if (req.body.supervisoremailid && req.body.password) {
            var supervisor = new Supervisor(req.body);
            supervisor.checkLogin(supervisor, function(err, supervisor) {
                if (err) return res.json(error.SERVER_ERROR);
                if (supervisor) {
                    if (supervisor.isActive) {
                        var result = error.OK;
                        result.result_data = supervisor;
                        res.json(result);
                    } else {
                        res.json(error.USER_BLOCKED);
                    }
                } else {
                    res.json(error.ERROR_AUTHENTICATION);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS)
        }
    });

    //superadmin forgot password
    app.post('/superadminforgotpassword', function(req, res, next) {
        if (req.body.superadminemailid) {
            var superadmin = new SuperAdmin(req.body);
            superadmin.alreadyExist(superadmin, function(err, superadmins) {
                if (err) return res.json(error.SERVER_ERROR);
                if (superadmins.length > 0) { //found emailid
                    var newpwd = generatePassword(); // generate new password
                    superadmins[0].changePassword(newpwd, function(err, rslt) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        }
                        var mailOptions = {
                            to: req.body.superadminemailid,
                            from: 'hitesh@mg.xpertuniversity.com',
                            subject: 'Use new Password',
                            message: getNewPasswordMailTemplate(newpwd),
                            altText: 'plain text'
                        };
                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                            // ...
                            if (err) {
                                res.json(error.ERROR_VARIFICATION_EMAIL);
                            } else {
                                res.json(error.FORGOT_EMAIL_SEND_SUCCESS);
                            }
                        });
                    });
                } else {
                    res.json(error.ERROR_EMAIL_NOT_FOUND)
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/addCompanies1', function(req, res, next) {
        var mailOptions = {
            to: req.body.email,
            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
            subject: 'Registration to CommScope',
            message: getSupervisorRegistrationMailTemplate(req.host, req.body),
            altText: 'plain text'
        };
        sesClient.sendEmail(mailOptions, function(err, data, resp) {
            if (err) {
                res.json(error.ERROR_VARIFICATION_EMAIL);
            } else {
                res.json(error.SERVER_ERROR);
            }
        });
    });

    app.post('/addCompanies', function(req, res, next) {
        if (req.body.companyname && req.body.username && req.body.email && req.body.password && req.body.isadmin && req.body.defaultreviewer) {
            var user = {};
            user.name = req.body.username;
            user.email = req.body.email;
            user.password = req.body.password;

            // add admin or reviwerrole
            if (req.body.isadmin == 'true') {
                user['role'] = "admin";
            } else {
                user['role'] = "reviewer";
            }

            // add default reviwer
            if (req.body.defaultreviewer == 'true') {
                user['defaultreviewer'] = "true";
            } else {
                user['defaultreviewer'] = "false";
            }
            if (req.body.companyid) {
                // res.json(error.SERVER_ERROR);
                var query = Company.findById(req.body.companyid);
                query.exec(function(err, company) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var includedReviewer = false;

                    if (company.users && company.users.length > 0 && req.body.isadmin != 'true') {
                        for (var i = 0; i < company.users.length; i++) {
                            // if (company.users[i].role == 'reviewer' && company.users[i].isActive) {
                            if (company.users[i].role == 'reviewer') {
                                includedReviewer = true;
                                break;
                            }
                        }
                    }

                    if (includedReviewer) {
                        if (req.body.defaultreviewer == 'true') {
                            // if(user['defaultreviewer'] == 'true'){
                            var alreadyExist = false;
                            for (var i = 0; i < company.users.length; i++) {
                                if (company.users[i].email == req.body.email) {
                                    alreadyExist = true;
                                }
                            }
                            if (alreadyExist) {
                                res.json(error.EMAIL_ID_ALREADY_EXITS);
                            } else {
                                for (var i = 0; i < company.users.length; i++) {
                                    // if(company.users[i].role == 'reviewer' && company.users[i].defaultreviewer == "true"){
                                    if (company.users[i].role == 'reviewer') {
                                        // company.users[i].name = req.body.username;
                                        // company.users[i].email = req.body.email;
                                        // company.users[i].password = req.body.password;
                                        company.users[i].defaultreviewer = "false";
                                        break;
                                    }
                                }
                                company.users.push(user);
                                company.save(function(err) {
                                    if (err) {
                                        res.json(error.SERVER_ERROR);
                                    } else {
                                        var mailOptions = {
                                            to: req.body.email,
                                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                            subject: 'Registration to CommScope',
                                            message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                                            altText: 'plain text'
                                        };
                                        sesClient.sendEmail(mailOptions, function(err, data, resp) {
                                            if (err) {
                                                res.json(error.ERROR_VARIFICATION_EMAIL);
                                            } else {
                                                res.json(error.OK);
                                            }
                                        });
                                    }
                                })
                            }
                        } else {
                            res.json(error.REVIWER_ALREADY_EXITS);
                        }
                    } else {
                        var isExists = false;
                        for (var i = 0; i < company.users.length; i++) {
                            if (company.users[i].email == user.email) {
                                isExists = true;
                                break;
                            }
                        }

                        if (isExists) {
                            res.json(error.EMAIL_ID_ALREADY_EXITS);
                        } else {
                            Company.update({
                                _id: req.body.companyid
                            }, {
                                $push: {
                                    users: user
                                }
                            }, function(err, result) {
                                var mailOptions = {
                                    to: req.body.email,
                                    from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                    subject: 'Registration to CommScope',
                                    message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                                    altText: 'plain text'
                                };
                                sesClient.sendEmail(mailOptions, function(err, data, resp) {
                                    if (err) {
                                        res.json(error.ERROR_VARIFICATION_EMAIL);
                                    } else {
                                        res.json(error.OK);
                                    }
                                });
                            });
                        }
                    }
                });
            } else {
                var company = new Company(req.body);
                company.users[0] = user;
                Company.create(company, function(err, company) {
                    if (err) return res.json(error.SERVER_ERROR);
                    var result = error.EMAIL_SEND_SUCCESS;
                    // res.json(error.EMAIL_SEND_SUCCESS);
                    // result.result_data = company;
                    var mailOptions = {
                        to: req.body.email,
                        from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                        subject: 'Registration to CommScope',
                        message: getSupervisorRegistrationMailTemplate(req.host, req.body),
                        altText: 'plain text'
                    };
                    sesClient.sendEmail(mailOptions, function(err, data, resp) {
                        if (err) {
                            res.json(error.ERROR_VARIFICATION_EMAIL);
                        } else {
                            res.json(error.OK);
                        }
                    });
                });
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    //get all companies
    app.get('/getcompanies', function(req, res, next) {
        Company.find({}, {
            _id: '_id',
            companyname: 'companyname',
            isActive: 'isActive',
            users: "users"
        }, function(err, companies) {
            if (err) {
                return res.json(error.SERVER_ERROR);
            } else {
                var responseObj = error.DATA_FOUND;
                responseObj.result_data = companies;
                res.json(responseObj);
            }
        });
    });

    //delete company
    app.post('/deletecompany', function(req, res, next) {
        if (req.body.companyid) {
            Company.remove({
                _id: req.body.companyid
            }, function(err) {
                if (err) {
                    res.json(error.SERVER_ERROR);
                } else {
                    res.json(error.OK);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/changeCompanyState', function(req, res, next) {
        if (req.body.companyid) {
            var company = new Company(req.body);
            company.alreadyExist(req.body, function(err, companies) {
                if (err) return res.json(error.SERVER_ERROR);
                if (companies.length > 0) {
                    companies[0].changeCompanyState(function(err, result) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        } else {
                            res.json(error.OK);
                        }
                    });
                } else {
                    res.json(error.DATA_NOT_FOUND)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/updateCompany', function(req, res, next) {
        if (req.body.companyid && req.body.companyname && req.body.revieweremail && req.body.reviewerid && req.body.reviewername) {
            // var company = new Company(req.body);
            Company.findById(req.body.companyid, function(err, company) {
                if (err) return res.json(error.SERVER_ERROR);
                if (company) {
                    company.companyname = req.body.companyname;
                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i].role == 'reviewer' && company.users[i]._id == req.body.reviewerid) {
                            company.users[i].name = req.body.reviewername;
                            company.users[i].email = req.body.revieweremail;
                            break;
                        }
                    }
                    company.save(function(err) {
                        if (err) {
                            res.json(error.SERVER_ERROR);
                        } else {
                            res.json(error.OK);
                        }
                    })
                } else {
                    res.json(error.DATA_NOT_FOUND);
                }
                // if (companies.length > 0) {
                //
                //     // companies[0].changeCompanyName(req.body, function(err, result) {
                //     //     if (err) {
                //     //         res.json(error.SERVER_ERROR);
                //     //     } else {
                //     //         res.json(error.OK);
                //     //     }
                //     // });
                // } else {
                //     res.json(error.DATA_NOT_FOUND)
                // }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });


    app.post('/getreviewer', function(req, res, next) {
        if (req.body.all) {
            if (req.body.companyid && req.body.all == 'false') {
                var company = new Company(req.body);
                company.alreadyExist(req.body, function(err, companies) {
                    if (err) return res.json(error.SERVER_ERROR);
                    if (companies[0].users.length > 0) {
                        var reviewer = {};
                        var found = false;
                        for (var i = 0; i < companies[0].users.length; i++) {
                            if (companies[0].users[i].role == 'reviewer') {
                                reviewer = companies[0].users[i];
                                found = true;
                                break;
                            }
                        }

                        if (found) {
                            var responseObj = error.OK;
                            responseObj.result_data = {};
                            responseObj.result_data = reviewer;
                            res.json(responseObj);
                        } else {
                            res.json(error.DATA_NOT_FOUND);
                        }
                    } else {
                        res.json(error.DATA_NOT_FOUND)
                    }
                });

            } else {
                Company.aggregate([{
                        "$unwind": "$users"
                    },
                    {
                        "$match": {
                            "users.role": "reviewer"
                        }
                    },
                    {
                        "$project": {
                            "reviewerid": "$users._id",
                            "companyname": "$companyname",
                            "reviewername": "$users.name",
                            "revieweremail": "$users.email",
                            "isActive": "$users.isActive",
                            "createdat": {
                                $dateToString: {
                                    format: "%d/%m/%Y",
                                    date: "$users.createdat"
                                }
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                })
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/updatereviewerdetail', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid && req.body.reviewername && req.body.email) {
            Company.findById(req.body.companyid, function(err, company) {
                if (err) {
                    res.json(error.SERVER_ERROR)
                } else {

                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i].role == 'reviewer' && company.users[i]._id == req.body.reviewerid) {
                            company.users[i].name = req.body.reviewername;
                            company.users[i].email = req.body.email;
                            company.save(function(err) {
                                res.json(error.OK);
                            })
                            break;
                        }
                    }
                    // res.json(company)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    })

    app.post('/updatereviewerstate', function(req, res, next) {
        if (req.body.companyid && req.body.reviewerid) {
            Company.findById(req.body.companyid, function(err, company) {
                if (err) {
                    res.json(error.SERVER_ERROR)
                } else {

                    for (var i = 0; i < company.users.length; i++) {
                        if (company.users[i].role == 'reviewer' && company.users[i]._id == req.body.reviewerid) {
                            company.users[i].isActive = !company.users[i].isActive;
                            company.save(function(err) {
                                res.json(error.OK);
                            })
                            break;
                        }
                    }
                    // res.json(company)
                }
            });

        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/getsubmissions', function(req, res, next) {
        if (req.body.all) {
            if (req.body.companyid && req.body.all == 'false') {
                User.aggregate([{
                        $lookup: {
                            from: "companies",
                            localField: "companyid",
                            foreignField: "_id",
                            as: "company"
                        }
                    },
                    {
                        $sort: {
                            createdat: -1
                        }
                    },
                    {
                        "$match": {
                            "companyid": new mongoose.Types.ObjectId(req.body.companyid)
                        }
                    },
                    {
                        "$project": {
                            "isActive": "$isActive",
                            "verified": "$verified",
                            "isChecked": "$isChecked",
                            "emailaddress": "$emailaddress",
                            "lastname": "$lastname",
                            "firstname": "$firstname",
                            "companyid": "$companyid",
                            "companyname": "$company.companyname",
                            "company": "$company",
                            "createdat": {
                                $dateToString: {
                                    format: "%m/%d/%Y %H:%M:%S",
                                    date: "$createdat"
                                }
                            },
                            "date": {
                                $dateToString: {
                                    format: "%m/%d/%Y",
                                    date: "$createdat"
                                }
                            },
                            "stepslength": {
                                $size: "$steps"
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                });

            } else {
                User.aggregate([{
                        $lookup: {
                            from: "companies",
                            localField: "companyid",
                            foreignField: "_id",
                            as: "company"
                        }
                    },
                    {
                        $sort: {
                            createdat: -1
                        }
                    },
                    {
                        "$project": {
                            "isActive": "$isActive",
                            "verified": "$verified",
                            "isChecked": "$isChecked",
                            "emailaddress": "$emailaddress",
                            "lastname": "$lastname",
                            "firstname": "$firstname",
                            "companyid": "$companyid",
                            "companyname": "$company.companyname",
                            "company": "$company",
                            "createdat": {
                                $dateToString: {
                                    format: "%m/%d/%Y %H:%M:%S",
                                    date: "$createdat"
                                }
                            },
                            "date": {
                                $dateToString: {
                                    format: "%m/%d/%Y",
                                    date: "$createdat"
                                }
                            },
                            "stepslength": {
                                $size: "$steps"
                            }
                        }
                    }
                ], function(err, result) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    }
                    var responseObj = error.OK;
                    responseObj.result_data = result;
                    res.json(responseObj);
                });
            }
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/getsubmissionsbyuser', function(req, res, next) {
        if (req.body.userid) {
            User.find({
                _id: req.body.userid
            }, function(err, result) {
                if (err) {
                    res.json(error.SERVER_ERROR);
                }
                if (result.length > 0) {
                    var responseObj = error.OK;
                    responseObj.result_data = result[0];
                    res.json(responseObj);
                }
            });
        } else {
            res.json(error.MANDATORY_FIELDS);
        }
    });

    app.post('/submitsubmission', function(req, res, next) {
        if (req.body.userid && req.body.companyid && req.body.companyname && req.body.firstname && req.body.lastname &&
            req.body.steps) {
            User.findById(req.body.userid, function(err, user) {
                user.isChecked = true;
                var verified = true;
                for (var i = 0; i < req.body.steps.length; i++) {
                    user.steps[i] = req.body.steps[i];
                    if (verified && !req.body.steps[i].passfail) {
                        verified = false;
                    }
                }
                // verified means pass
                user.verified = verified;
                user.updatedat = new Date().toISOString();
                if (verified) {
                    user.passattempts = user.passattempts + 1;
                    htmlToPdf.convertHTMLString(getSubmissionPassTemplate(req.host, req.body), 'pdfs/' + user._id + '.pdf',
                        function(err, success) {
                            if (err) {
                                console.log('Oh noes! Errorz!');
                                console.log(error);
                                res.json(error.SERVER_ERROR);

                            } else {
                                console.log(user.emailaddress);
                                fs.readFile('pdfs/' + user._id + '.pdf', function(err, data) {
                                    if (err) {
                                        throw err;
                                    }
                                    var base64data = new Buffer(data, 'binary').toString('base64');
                                    var s3 = new AWS.S3();
                                    s3.putObject({
                                        Bucket: 'commspecmedia',
                                        Key: 'pdfs/' + user._id + '.pdf',
                                        Body: base64data
                                    }, function(resp) {
                                        console.log('Successfully uploaded package.');
                                        //  fs.unlink('./www/testpdf.pdf');
                                        var CRLF = '\r\n',
                                            ses = require('node-ses'),
                                            client = ses.createClient({
                                                key: 'AKIAIV4Q42UG5E2GH2IQ',
                                                secret: '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'
                                            }),
                                            rawMessage = [
                                                'From: "CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                                'To: ' + user.emailaddress + '',
                                                'Subject: Result for CommScope Submission',
                                                'Content-Type: multipart/mixed;',
                                                '    boundary="_003_97DCB304C5294779BEBCFC8357FCC4D2"',
                                                'MIME-Version: 1.0',
                                                '',
                                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                                'Content-Type: text/plain; charset="us-ascii"',
                                                'Content-Transfer-Encoding: quoted-printable',
                                                'Congratulations ' + user.firstname + ' ' + user.lastname + ',',
                                                '',
                                                'The submitted Drop Craft activity for installing F-Connectors on RG6 Tri-Shield drop cable has passed evaluation.',
                                                '',
                                                'Your CommScope Drop Craft Evaluation has passed.',
                                                'Good job!',
                                                'Please download and Print your Certificate of Completion.',
                                                'If you have any questions or issues, please contact your supervisor.',
                                                '',
                                                'The CommScope Training Team',
                                                '',
                                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                                'Content-Type: application/pdf; name="code.pdf"',
                                                'Content-Description: pdfs/testpdf.pdf',
                                                'Content-Disposition: attachment; filename="result.pdf"; size=4;',
                                                '    creation-date="Mon, 03 Aug 2015 11:39:39 GMT";',
                                                '    modification-date="Mon, 03 Aug 2015 11:39:39 GMT"',
                                                'Content-Transfer-Encoding: base64',
                                                '',
                                                '' + new Buffer(data, 'binary').toString('base64') + '',
                                                ''
                                            ].join(CRLF);

                                        // var base64data = new Buffer(data, 'binary').toString('base64');
                                        sesClient.sendRawEmail({
                                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                            rawMessage: rawMessage
                                        }, function(err, data, resp) {
                                            // ...
                                            fs.unlink('pdfs/' + user._id + '.pdf');
                                            user.save(function(err) {
                                                if (err) {
                                                    res.json(error.SERVER_ERROR);
                                                } else {
                                                    res.json(error.OK);
                                                }
                                            });
                                            // res.json(error.OK);
                                        });
                                    });

                                });
                            }
                        });
                } else {
                    user.failattempts = user.failattempts + 1;
                    user.isActive = false;
                    user.otp = generatePassword();
                    req.body.otp = user.otp;
                    // console.log("1:"+req.body.otp);
                    // console.log("2:"+user.otp);
                    var mailOptions = {
                        // to: user.email,
                        to: user.emailaddress,
                        from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                        subject: 'Result for CommScope Submission',
                        message: getSubmissionFailTemplate(req.host, req.body),
                        altText: 'plain text'
                    };
                    sesClient.sendEmail(mailOptions, function(err, data, resp) {
                        if (err) {
                            res.json(error.ERROR_VARIFICATION_EMAIL);
                        } else {
                            // console.log(user);
                            user.save(function(err) {
                                if (err) {
                                    res.json(error.SERVER_ERROR);
                                } else {
                                    res.json(error.OK);
                                }
                            });
                        }
                    });
                }
            })
        }
    });
    //
    app.get('/getstatistics', function(req, res, next) {
        User.aggregate([{
            "$project": {
                "_id": 0,
                "verified": "$verified",
                "isChecked": "$isChecked",
                "createdat": "$createdat",
                "passattempts": "$passattempts",
                "failattempts": "$failattempts",
                "stepslength": {
                    $size: "$steps"
                },
                "attempts": "$attempts"
            }
        }], function(err, result) {
            if (err) {
                res.json(error.SERVER_ERROR);
            }
            var responseObj = error.OK;
            responseObj.result_data = result;
            res.json(responseObj);
        });
    });


    app.get('/uploadpdf', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        fs.readFile('pdfs/testpdf.pdf', function(err, data) {
            if (err) {
                throw err;
            }

            var base64data = new Buffer(data, 'binary').toString('base64');

            var s3 = new AWS.S3();
            s3.putObject({
                Bucket: 'commspecmedia',
                Key: 'pdfs/testpdf.pdf',
                Body: base64data
            }, function(resp) {
                console.log('Successfully uploaded package.');
                //  fs.unlink('./www/testpdf.pdf');
            });

        });
    });

    app.get('/createpdf', function(req, res, next) {
        htmlToPdf.convertHTMLString(getSubmissionPassTemplate(req.host, req.body), 'pdfs/testpdf.pdf',
            function(err, success) {
                if (err) {
                    console.log('Oh noes! Errorz!');
                    console.log(error);
                    res.json(error.SERVER_ERROR);

                } else {
                    fs.readFile('pdfs/testpdf.pdf', function(err, data) {
                        // res.json(data);

                        var CRLF = '\r\n',
                            ses = require('node-ses'),
                            client = ses.createClient({
                                key: 'AKIAIV4Q42UG5E2GH2IQ',
                                secret: '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'
                            }),
                            rawMessage = [
                                'From: "CommScope Training" <hitesh@mg.xpertuniversity.com>',
                                'To: testac1313@gmail.com',
                                'Subject: greetings',
                                'Content-Type: multipart/mixed;',
                                '    boundary="_003_97DCB304C5294779BEBCFC8357FCC4D2"',
                                'MIME-Version: 1.0',
                                '',
                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                'Content-Type: text/plain; charset="us-ascii"',
                                'Content-Transfer-Encoding: quoted-printable',
                                'Hi brozeph,',
                                '',
                                'I have attached a code file for you.',
                                '',
                                'Cheers.',
                                '',
                                '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
                                'Content-Type: application/pdf; name="code.pdf"',
                                'Content-Description: pdfs/testpdf.pdf',
                                'Content-Disposition: attachment; filename="certificate.pdf"; size=4;',
                                '    creation-date="Mon, 03 Aug 2015 11:39:39 GMT";',
                                '    modification-date="Mon, 03 Aug 2015 11:39:39 GMT"',
                                'Content-Transfer-Encoding: base64',
                                '',
                                '' + new Buffer(data, 'binary').toString('base64') + '',
                                ''
                            ].join(CRLF);

                        // var base64data = new Buffer(data, 'binary').toString('base64');
                        sesClient.sendRawEmail({
                            from: '"CommScope Training" <hitesh@mg.xpertuniversity.com>',
                            rawMessage: rawMessage
                        }, function(err, data, res) {
                            // ...
                        });
                    });
                    // res.json(error.OK);

                }
            }
        );
        // var PDF = require('pdfkit'); //including the pdfkit module
        // var fs = require('fs');
        // var text = 'ANY_TEXT_YOU_WANT_TO_WRITE_IN_PDF_DOC';
        // var filename = "testpdf";
        // doc = new PDF(); //creating a new PDF object
        // doc.pipe(fs.createWriteStream('pdfs/' + filename + '.pdf')); //creating a write stream
        // doc.image('images/ic_comscope_big.png', 150, 150);
        // doc.text(text, 150, 100); //adding the text to be written,
        // // more things can be added here including new pages
        // doc.end();

    });

    app.get('/dateformat', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        res.json(now);
    });

    app.get('/dateformat', function(req, res, next) {
        // Read in the file, convert it to base64, store to S3
        var dateFormat = require('dateformat');
        var now = new Date();
        now = dateFormat(now, "mmmm dd, yyyy");
        res.json(now);
    });

    app.get('/updateResult', function(req, res, next) {
        var userid = "5885919ea696500a201ba69e";
        var companyid = "58830988b9c5f808fc307ec4";
        var emailaddress = "pradipkachhadiya.ibl@gmail.com";
        var pass = true;
        Result.find({
            userid: userid,
            companyid: companyid,
            emailaddress: emailaddress
        }, function(err, results) {
            if (results.length > 0) {
                if (pass) {
                    results[0].passattempts = results[0].passattempts + 1;
                } else {
                    results[0].failattempts = results[0].failattempts + 1;
                }
                results[0].updatedat = new Date().toISOString();
                results[0].attempts = results[0].attempts + 1;              
                results[0].save(function(err) {
                    if (err) {
                        res.json(error.SERVER_ERROR);
                    } else {
                        res.json(error.OK);
                    }
                });
            } else {
                var tempresult = {};
                tempresult.userid = userid;
                tempresult.companyid = companyid,
                    tempresult.emailaddress = emailaddress;
                var result = new Result(tempresult);
                Result.create(result, function(err, result) {
                    if (err) res.json(error.SERVER_ERROR);
                    res.json(result);
                });
            }
        });
    });

};
