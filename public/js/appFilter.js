comscopeApp.filter('verifiedFilter', function() {
        return function(evaluations, verified) {
            var filterevaluations = [];
            if (!verified) {
                return evaluations;
            }
            var verifiedType = verified.type.toLowerCase();
            if (!verifiedType || '' === verifiedType) {
                return evaluations;
            }
            // for (var i = 0; i < evaluations.length; i++) {
            //     var evaluation = evaluations[i];
            //     arrEvaluationverified = "" + evaluation.verified;
            //     if (arrEvaluationverified == verifiedType) {
            //         filterevaluations.push(evaluation);
            //     }
            // }
            for (var i = 0; i < evaluations.length; i++) {
                var evaluation = evaluations[i];
                arrEvaluationverified = "" + evaluation.isChecked;
                if (arrEvaluationverified == verifiedType) {
                    filterevaluations.push(evaluation);
                }
            }
            return filterevaluations;
        };
    })
    .filter('capitalize', function() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    });
