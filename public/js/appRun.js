comscopeApp.run(function($window, $rootScope, $state, $location) {
    $rootScope.online = navigator.onLine;
    $rootScope.isLoader = false;
    $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
            $rootScope.online = false;
        });
    }, false);
    $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
            $rootScope.online = true;
        });
    }, false);

    $rootScope.user = angular.fromJson($window.localStorage.user) || {};
    // $rootScope.supervisor = angular.fromJson($window.localStorage.supervisor) || {};
    // $rootScope.superadmin = angular.fromJson($window.localStorage.superadmin) || {};
    // if(angular.fromJson($window.localStorage.supervisor) || angular.fromJson($window.localStorage.superadmin)){
    //   $rootScope.role = $window.localStorage.role;
    // }
    // console.log($rootScope.supervisor);


    $(document).on('click', '.btn-collapse-sidebar-left', function() {
        $(".sidebar-left").toggleClass("toggle");
        $(".page-content").toggleClass("toggle");
    });
});
