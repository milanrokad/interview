comscopeApp.config(function ($stateProvider, $urlRouterProvider, $routeProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/slotview');
    // $urlRouterProvider.otherwise('/home/dashboard');

    // $locationProvider.html5Mode(true);
    // $locationProvider.hashPrefix = '!';

    $stateProvider
        // My Demo Start
        .state('Mylogin', {
            url: '/login',
            cache: false,
            templateUrl: '../views/MyDemoPages/Accounts/login.html',
            controller: 'AccountCtrl'
            // onEnter:function($rootScope,$state,$window){
            //   if($rootScope.loggedIn || $rootScope.loggedIn == 'true'){
            //     $state.go('home.submissions');
            //   }
            // }
        })
        .state('SlotView', {
            url: '/slotview',
            cache: false,
            templateUrl: '../views/MyDemoPages/SlotView/slotview.html',
            controller: 'slotviewCtrt'
            // onEnter:function($rootScope,$state,$window){
            //   if($rootScope.loggedIn || $rootScope.loggedIn == 'true'){
            //     $state.go('home.submissions');
            //   }
            // }
        });


}).filter('genderFilter', function () {
    return function (users, gender) {
        var filterUsers = [];
        if (!gender) {
            return users;
        }
        var genderType = gender.type.toLowerCase();
        if (!genderType || '' === genderType) {
            return users;
        }
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            console.log(user.gender.toLowerCase() + ":" + genderType);
            if (arrUserGender.indexOf(genderType) !== -1) {
                filterUsers.push(user);
            }
        }
        return filterUsers;
    };
});
