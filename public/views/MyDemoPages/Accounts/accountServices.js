(function() {
    'use strict';
    angular
        .module('comscopeapp')
        .factory('AcService', acService);

    function acService($http, $rootScope, $q,apiUrl) { // , apiUrl, CommonService
        return {
            UserSignUp: function(obj) {
                var deferred = $q.defer();
                var success = function(response, status, headers, config) {
                    deferred.resolve(response);
                }
                var error = function(error, status, headers, config) {
                    deferred.reject(error);
                }
                $http.post(apiUrl + '/signup',obj).success(success).error(error);
                return deferred.promise;
            },
            UserLogin: function(obj) {
                var deferred = $q.defer();
                var success = function(response, status, headers, config) {
                    deferred.resolve(response);
                }
                var error = function(error, status, headers, config) {
                    deferred.reject(error);
                }
                $http.post(apiUrl + '/login',obj).success(success).error(error);
                return deferred.promise;
            }
        }
    }
})();
