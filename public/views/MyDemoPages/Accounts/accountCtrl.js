(function() {
    'use strict';
    angular
        .module('comscopeapp')
        .controller('AccountCtrl', AccountCtrl);

    function AccountCtrl($scope, $state, $rootScope,AcService,CommonServices,$timeout) {
        $scope.isLogin = true;
        console.log("wow");
        $scope.signupModel = {};
        $scope.loginModel = {};
        $scope.switchTab = function(key) {
            if (key == '0') {
                $scope.isLogin = true;
            }
            if (key == '1') {
                $scope.isLogin = false;
            }
        };

        $scope.UserSignUp = function(dataModel) {
            console.log(dataModel);
            CommonServices.showLoader();
            var success = function(response, status, headers, config) {
                console.log(response);
                $timeout(function () {
                  CommonServices.hideLoader();
                }, 1000);
            }
            // error callback
            var error = function(error, status, headers, config) {
                CommonServices.hideLoader();
                console.log(error);
            }
            AcService.UserSignUp(dataModel).then(success, error);
        };
        $scope.UserLogin = function(dataModel) {
            console.log(dataModel);
            CommonServices.showLoader();
            var success = function(response, status, headers, config) {
                console.log(response);
                $timeout(function () {
                  CommonServices.hideLoader();
                }, 2000);
            }
            // error callback
            var error = function(error, status, headers, config) {
                CommonServices.hideLoader();
                console.log(error);
            }
            AcService.UserLogin(dataModel).then(success, error);
        };
    }
})();
