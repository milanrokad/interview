(function () {
    'use strict';
    angular
        .module('comscopeapp')
        .controller('slotviewCtrt', slotviewCtrt)
        .directive('slotList', function () {
            return {
                template: `
                <div id="contact">
                    <h3>Slot List</h3>
                    <div class="card card-outer" ng-repeat="slot in slotData" style="width: 18rem;" ng-click="gotoDetail(slot)"
                        ng-class="slot.haveDetail?'red':''">
                        <div class="card-body">
                            <h5 class="card-title" ng-bind="slot.slot"></h5>
                        </div>
                    </div>
                </div>`
            };
        })
        .directive('detailForm', function () {
            return {
                template: `
                <div class="container">
                    <form id="contact" class="w-100p" ng-submit="saveDetail(reqModel)">
                        <h3>Slot Detail</h3>
                        <p class="back" ng-click="backToList()">
                            <img src="img/backicon.png" />
                        </p>
                        <fieldset>
                            <input placeholder="First name" ng-model="reqModel.f_name" type="text" tabindex="1">
                        </fieldset>
                        <fieldset>
                            <input placeholder="Last Name" ng-model="reqModel.l_name" type="text" tabindex="2">
                        </fieldset>
                        <fieldset>
                            <input placeholder="Your Phone Number" ng-model="reqModel.contact" type="text" tabindex="3">
                        </fieldset>
                        <fieldset>
                            <div class="col-md-12" style="margin-top:10px;">
                                <div class="col-md-6">
                                    <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Save</button>
                                </div>
                                <div class="col-md-6">
                                    <button ng-click="backToList()" class="danger">Cancel</button>
                                </div>
                            </div>
                        </fieldset>
            
                    </form>
                </div>`
            };
        });
    function slotviewCtrt($scope, $state, $rootScope, SlotviewServices, CommonServices, $timeout) {
        $scope.isList = true;
        $scope.reqModel = {};
        $scope.switchTab = function (key) {
            if (key == '0') {
                $scope.isLogin = true;
            }
            if (key == '1') {
                $scope.isLogin = false;
            }
        };

        $scope.$on('$ionicView.beforeEnter', function () {
            $scope.getSlotsDetail();
        });

        $scope.gotoDetail = function (data) {
            $scope.reqModel = data;
            $scope.isList = false;
        }
        $scope.backToList = function () {
            $scope.getSlotsDetail();
            $scope.isList = true;
        }

        $scope.saveDetail = function (reqMode) {
            CommonServices.showLoader();
            var success = function (response, status, headers, config) {
                    $scope.backToList();
                $scope.slotData = response.responseData;
                CommonServices.hideLoader();
            }
            // error callback
            var error = function (error, status, headers, config) {
                CommonServices.hideLoader();
            }
            SlotviewServices.saveDetail(reqMode).then(success, error);
        };

        $scope.getSlotsDetail = function () {
            CommonServices.showLoader();
            var success = function (response, status, headers, config) {
                if (response.responseCode == 0) {
                    $scope.slotData = response.responseData;
                }
                CommonServices.hideLoader();
            }
            // error callback
            var error = function (error, status, headers, config) {
                CommonServices.hideLoader();
                console.log(error);
            }
            SlotviewServices.GetSlots().then(success, error);
        };
        $scope.getSlotsDetail();
    }
})();
