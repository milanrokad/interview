(function() {
    'use strict';
    angular
        .module('comscopeapp')
        .factory('SlotviewServices', SlotviewServices);

    function SlotviewServices($http, $rootScope, $q,apiUrl) { // , apiUrl, CommonService
        return {
            GetSlots: function(obj) {
                var deferred = $q.defer();
                var success = function(response, status, headers, config) {
                    deferred.resolve(response);
                }
                var error = function(error, status, headers, config) {
                    deferred.reject(error);
                }
                $http.get(apiUrl + 'getSlots').success(success).error(error);
                return deferred.promise;
            },
            
            saveDetail: function(reqModel) {
                var deferred = $q.defer();
                var success = function(response, status, headers, config) {
                    deferred.resolve(response);
                }
                var error = function(error, status, headers, config) {
                    deferred.reject(error);
                }
                $http.post(apiUrl + 'saveDetail',reqModel).success(success).error(error);
                return deferred.promise;
            },
            
        }
    }
})();
