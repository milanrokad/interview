(function() {
    'use strict';
    angular
        .module('comscopeapp')
        .factory('CommonServices', commonServices);

    function commonServices($http, $rootScope, $q, apiUrl) { // , apiUrl, CommonService
        return {
            showLoader: function() {
                $rootScope.isLoader = true;
            },
            hideLoader: function() {
                $rootScope.isLoader = false;
            }
        }
    }
})();
